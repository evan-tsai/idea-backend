@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '聯絡設定',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('contact.save', $customer) }}" novalidate>
                {{ csrf_field() }}

                <div class="card">
                    <div class="card-header">
                        聯絡設定
                    </div>

                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a href="#contact"
                                   id="contact-tab"
                                   class="nav-link active"
                                   data-toggle="tab"
                                   role="tab"
                                   aria-controls="contact"
                                   aria-selected="true">聯絡資訊</a>
                            </li>
                            <li class="nav-item">
                                <a href="#email"
                                   id="email-tab"
                                   class="nav-link"
                                   data-toggle="tab"
                                   role="tab"
                                   aria-controls="email"
                                   aria-selected="true">Email</a>
                            </li>
                        </ul>
                        <div class="tab-content mb-2" id="myTabContent">
                            <div class="form-group row tab-pane active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">電話</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'contact',
                                                'columns' => ['fields', 'phone']
                                            ])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">電話2</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'contact',
                                                'columns' => ['fields', 'phone2']
                                            ])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">傳真</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'contact',
                                                'columns' => ['fields', 'fax']
                                            ])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">傳真2</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'contact',
                                                'columns' => ['fields', 'fax2']
                                            ])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">電子信箱</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'contact',
                                                'columns' => ['fields', 'email']
                                            ])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">地址</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'contact',
                                                'columns' => ['fields', 'address']
                                            ])
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Social</label>

                                        <div class="col-sm-10 p-2">
                                            <model-select
                                                    :models="{{ $socials }}"
                                                    model-label="type"
                                                    field="socials"
                                                    :selected-id="{{ json_encode(old('socials', $selectedSocials)) }}">
                                            </model-select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row tab-pane" id="email" role="tabpanel" aria-labelledby="email-tab">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">啟動功能</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'email',
                                                'columns' => ['enable']
                                            ])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">使用者設定收件人</label>
                                        <div class="col-sm-10 p-2">
                                            @include('partials.form.setting-switch', [
                                                'setting' => 'contact_settings',
                                                'name' => 'email',
                                                'columns' => ['receivers', 'enable']
                                            ])
                                        </div>
                                    </div>
                                    <multi-labeler :labels="{{ setting('email.receivers.items', $customer->id, config('default.contact_settings.email.receivers.items')) }}"
                                                   name="email[receivers][items]"
                                                   type="email"
                                                   title="Email"
                                    ></multi-labeler>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection