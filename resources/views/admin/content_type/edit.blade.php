@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '內容產生' => route('content-type.index', $customer),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('content-type.update', [$customer, $contentType]) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('admin.content_type._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection