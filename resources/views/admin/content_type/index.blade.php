@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '內容產生',
    ]])

    <table-component :fields="{{ $fields }}"
                     base-url="{{ route('content-type.index', $customer) }}"
                     api-url="{{ route('api.admin.content-type.get', $customer) }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection