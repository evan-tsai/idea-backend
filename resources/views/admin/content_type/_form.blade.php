<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
                class="form-control @formValidClass('name')"
                required
                value="{{ old('name', $contentType->name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">類型 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="type"
               class="form-control @formValidClass('type')"
               required
               value="{{ old('type', $contentType->type) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'type'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">排序</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $contentType->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>

<div class="card">
    <div class="card-header">
        設定
    </div>

    <div class="card-body">
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="#c-field"
                       id="c-field-tab"
                       class="nav-link active"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="c-field"
                       aria-selected="true">欄位</a>
                </li>
                <li class="nav-item">
                    <a href="#c-image"
                       id="c-image-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="c-image"
                       aria-selected="true">代表圖</a>
                </li>
                <li class="nav-item">
                    <a href="#c-editor"
                       id="c-editor-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="c-editor"
                       aria-selected="true">編輯器</a>
                </li>
            </ul>

            <div class="tab-content mb-2" id="myTabContent">
                <div class="form-group row tab-pane active" id="c-field" role="tabpanel" aria-labelledby="c-field-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">描述</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['fields', 'description']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">連結</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['fields', 'url']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">排序</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['fields', 'sort']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="c-image" role="tabpanel" aria-labelledby="c-image-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['image', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['image', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['image', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="c-editor" role="tabpanel" aria-labelledby="c-editor-tab">
                    <div class="col-sm-12 m-2">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'content_settings',
                                    'name' => 'content',
                                    'type' => $contentType->type,
                                    'columns' => ['editors', 'enable']
                                ])
                            </div>
                        </div>

                        <multi-labeler :labels="{{ setting('content.editors.labels', $customer->id, config('default.content_settings.content.editors.labels'), $contentType->type) }}"
                                       name="content[editors][labels]"
                        ></multi-labeler>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>