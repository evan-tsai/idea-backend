@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '內容產生' => route('content-type.index', $customer),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('content-type.store', $customer) }}">
                {{ csrf_field() }}

                @include('admin.content_type._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection