@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '客戶',
    ]])

    <table-component :fields="{{ $fields }}"
                     base-url="{{ route('customer.index') }}"
                     api-url="{{ route('api.admin.customers.get') }}"
                     api-token="{{ auth()->user()->api_token }}">
        <div slot="custom-actions" slot-scope="{ row, fnEdit, fnDelete }">
            <div v-cloak>
                <a :href="fnEdit(row.item.id)" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                <button v-if="row.item.id !== 1" type="submit" class="btn btn-danger btn-sm mx-1" @click.prevent="fnDelete(row.item.id)"><i class="fa fa-trash"></i></button>
            </div>
        </div>
    </table-component>
@endsection