@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '客戶' => route('customer.index'),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('customer.update', $customer) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('admin.customer._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection