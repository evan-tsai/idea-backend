<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
                class="form-control @formValidClass('name')"
                required
                value="{{ old('name', $customer->name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">網域 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="domain"
                class="form-control @formValidClass('domain')"
                required
                value="{{ old('domain', $customer->domain) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'domain'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">預設語言 @requiredInput</label>
    <div class="col-sm-10">
        <select name="default_lang" class="form-control" required>
            @foreach ($languages as $language)
                <option value="{{ $language->id }}"
                    @if(old('default_lang', $customer->default_lang) === $language->id) selected @endif>
                    {{ $language->name }}
                </option>
            @endforeach
        </select>

        @include('partials.form.invalid-feedback', ['field' => 'default_lang'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">啟用語言 @requiredInput</label>
    <div class="col-sm-10">
        <div>
            <model-select
                    :multiple="true"
                    field="languages"
                    :selected-id="{{ json_encode(old('languages', $customer->languages->pluck('id'))) }}"
                    :models="{{ $languages }}">
            </model-select>
        </div>

        @include('partials.form.invalid-feedback', ['field' => 'languages'])
    </div>
</div>