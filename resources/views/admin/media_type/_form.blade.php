<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
                class="form-control @formValidClass('name')"
                required
                value="{{ old('name', $mediaType->name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">類型 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="type"
               class="form-control @formValidClass('type')"
               required
               value="{{ old('type', $mediaType->type) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'type'])
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label">圖示</label>
    <div class="col-sm-10">
        <font-awesome-select icon="{{ old('icon', $mediaType->icon) }}"></font-awesome-select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">排序</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $mediaType->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>

<div class="card">
    <div class="card-header">
        設定
    </div>

    <div class="card-body">
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="#m-field"
                       id="m-field-tab"
                       class="nav-link active"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="m-field"
                       aria-selected="true">欄位</a>
                </li>
                <li class="nav-item">
                    <a href="#m-image"
                       id="m-image-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="m-image"
                       aria-selected="true">代表圖</a>
                </li>
            </ul>

            <div class="tab-content mb-2" id="myTabContent">
                <div class="form-group row tab-pane active" id="m-field" role="tabpanel" aria-labelledby="m-field-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">描述</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['fields', 'description']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">連結</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['fields', 'url']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">排序</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['fields', 'sort']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">類別排序</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['category', 'sort']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="m-image" role="tabpanel" aria-labelledby="m-image-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['image', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['image', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'media_settings',
                                    'name' => 'media',
                                    'type' => $mediaType->type,
                                    'columns' => ['image', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>