@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '媒體類別',
    ]])

    <table-component :fields="{{ $fields }}"
                     base-url="{{ route('media-type.index', $customer) }}"
                     api-url="{{ route('api.admin.media-type.get', $customer) }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection