@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '媒體類別' => route('media-type.index', $customer),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('media-type.store', $customer) }}">
                {{ csrf_field() }}

                @include('admin.media_type._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection