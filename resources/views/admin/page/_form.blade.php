<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
               class="form-control @formValidClass('name')"
               required
               value="{{ old('name', $page->name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">類型 @requiredInput</label>
    <div class="col-sm-10">
        <slug-widget class="form-control @formValidClass('type')" name="type" old-slug="{{ old('type', $page->type) }}"></slug-widget>

        @include('partials.form.invalid-feedback', ['field' => 'type'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">排序</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $page->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>

<div class="card">
    <div class="card-header">
        設定
    </div>

    <div class="card-body">
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="#p-field"
                       id="p-field-tab"
                       class="nav-link active"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-field"
                       aria-selected="true">欄位</a>
                </li>
                <li class="nav-item">
                    <a href="#p-banner"
                       id="p-banner-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-banner"
                       aria-selected="true">橫幅</a>
                </li>
                <li class="nav-item">
                    <a href="#p-gallery"
                       id="p-gallery-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-gallery"
                       aria-selected="true">相簿</a>
                </li>
                <li class="nav-item">
                    <a href="#p-editor"
                       id="p-editor-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-editor"
                       aria-selected="true">編輯器</a>
                </li>
            </ul>

            <div class="tab-content mb-2" id="myTabContent">
                <div class="form-group row tab-pane active" id="p-field" role="tabpanel" aria-labelledby="p-field-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Seo</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['fields', 'seo']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-banner" role="tabpanel" aria-labelledby="p-banner-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'height']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Max</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'max']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'fields', 'name']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'fields', 'description']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Video Url</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'fields', 'video_url']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Url</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['banners', 'fields', 'url']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-gallery" role="tabpanel" aria-labelledby="p-gallery-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'height']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Max</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'max']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'fields', 'name']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'fields', 'description']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Video Url</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'fields', 'video_url']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Url</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->name,
                                    'columns' => ['gallery', 'fields', 'url']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-editor" role="tabpanel" aria-labelledby="p-editor-tab">
                    <div class="col-sm-12 m-2">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'page_settings',
                                    'name' => 'page',
                                    'type' => $page->type,
                                    'columns' => ['editors', 'enable']
                                ])
                            </div>
                        </div>

                        <multi-labeler :labels="{{ setting('page.editors.labels', $customer->id, config('default.page_settings.page.editors.labels'), $page->type) }}"
                                        name="page[editors][labels]"
                        ></multi-labeler>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>