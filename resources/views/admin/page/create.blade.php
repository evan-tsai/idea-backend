@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '頁面生產' => route('page.index', $customer),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('page.store', $customer) }}">
                {{ csrf_field() }}

                @include('admin.page._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection