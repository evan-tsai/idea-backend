@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '頁面生產' => route('page.index', $customer),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('page.update', [$customer, $page]) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('admin.page._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection