@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '頁面生產',
    ]])

    <table-component :fields="{{ $fields }}"
                     base-url="{{ route('page.index', $customer) }}"
                     api-url="{{ route('api.admin.page.get', $customer) }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection