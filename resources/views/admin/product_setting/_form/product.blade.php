<div class="card">
    <div class="card-header">
        產品
    </div>

    <div class="card-body">
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="#p-field"
                       id="p-field-tab"
                       class="nav-link active"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-field"
                       aria-selected="true">欄位</a>
                </li>
                <li class="nav-item">
                    <a href="#p-image"
                       id="p-image-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-image"
                       aria-selected="true">代表圖</a>
                </li>
                <li class="nav-item">
                    <a href="#p-icon"
                       id="p-icon-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-icon"
                       aria-selected="true">圖示</a>
                </li>
                <li class="nav-item">
                    <a href="#p-gallery"
                       id="p-gallery-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-icon"
                       aria-selected="true">輪播圖</a>
                </li>
                <li class="nav-item">
                    <a href="#p-editor"
                       id="p-editor-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="p-icon"
                       aria-selected="true">編輯器</a>
                </li>
            </ul>

            <div class="tab-content mb-2" id="myTabContent">
                <div class="form-group row tab-pane active" id="p-field" role="tabpanel" aria-labelledby="p-field-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟用功能</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Model</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['fields', 'model']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['fields', 'description']
                                ])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">製造日</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['fields', 'manufacture']
                                ])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">連結</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['fields', 'url']
                                ])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Stock</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['fields', 'stock']
                                ])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Seo</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['fields', 'seo']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-image" role="tabpanel" aria-labelledby="p-image-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['image', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['image', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['image', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-icon" role="tabpanel" aria-labelledby="p-icon-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['icon', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['icon', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['icon', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-gallery" role="tabpanel" aria-labelledby="p-gallery-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'height']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Max</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'max']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'fields', 'name']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'fields', 'description']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Video Url</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'fields', 'video_url']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Url</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['gallery', 'fields', 'url']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="p-editor" role="tabpanel" aria-labelledby="p-icon-tab">
                    <div class="col-sm-12 m-2">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product',
                                    'columns' => ['editors', 'enable']
                                ])
                            </div>
                        </div>

                        <multi-labeler :labels="{{ setting('product.editors.labels', $customer->id, config('default.product_settings.product.editors.labels')) }}"
                                        name="product[editors][labels]"
                        ></multi-labeler>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>