<div class="card">
    <div class="card-header">
        產品標籤
    </div>

    <div class="card-body">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">功能啟動</label>
            <div class="col-sm-10 p-2 pl-3">
                @include('partials.form.setting-switch', [
                    'setting' => 'product_settings',
                    'name' => 'product_tag',
                    'columns' => ['enable']
                ])
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10 p-2 pl-3">
                @include('partials.form.setting-switch', [
                    'setting' => 'product_settings',
                    'name' => 'product_tag',
                    'columns' => ['fields', 'description']
                ])
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">代表圖 啟動</label>
            <div class="col-sm-10 p-2 pl-3">
                @include('partials.form.setting-switch', [
                    'setting' => 'product_settings',
                    'name' => 'product_tag',
                    'columns' => ['image', 'enable']
                ])
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">代表圖 Width</label>
            <div class="col-sm-10">
                @include('partials.form.setting-input', [
                    'setting' => 'product_settings',
                    'name' => 'product_tag',
                    'columns' => ['image', 'width']
                ])
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">代表圖 Height</label>
            <div class="col-sm-10">
                @include('partials.form.setting-input', [
                    'setting' => 'product_settings',
                    'name' => 'product_tag',
                    'columns' => ['image', 'height']
                ])
            </div>
        </div>
    </div>
</div>