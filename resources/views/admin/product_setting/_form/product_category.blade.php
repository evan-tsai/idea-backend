<div class="card">
    <div class="card-header">
        產品類別
    </div>

    <div class="card-body">
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="#pc-field"
                       id="pc-field-tab"
                       class="nav-link active"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="pc-field"
                       aria-selected="true">欄位</a>
                </li>
                <li class="nav-item">
                    <a href="#pc-image"
                       id="pc-image-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="pc-image"
                       aria-selected="true">代表圖</a>
                </li>
                <li class="nav-item">
                    <a href="#pc-icon"
                       id="pc-icon-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="pc-icon"
                       aria-selected="true">圖示</a>
                </li>
            </ul>

            <div class="tab-content mb-2" id="myTabContent">
                <div class="form-group row tab-pane active" id="pc-field" role="tabpanel" aria-labelledby="pc-field-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Title</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['fields', 'title']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['fields', 'description']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Slug</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['fields', 'slug']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sort</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['fields', 'sort']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Seo</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['fields', 'seo']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="pc-image" role="tabpanel" aria-labelledby="pc-image-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['image', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['image', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['image', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="pc-icon" role="tabpanel" aria-labelledby="pc-icon-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['icon', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['icon', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'product_settings',
                                    'name' => 'product_category',
                                    'columns' => ['icon', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>