@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品設定',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product-setting.save', $customer) }}">
                {{ csrf_field() }}

                @include('admin.product_setting._form.product_category')
                @include('admin.product_setting._form.product')
                @include('admin.product_setting._form.product_tag')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection