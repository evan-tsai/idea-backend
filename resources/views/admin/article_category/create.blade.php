@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '文章類別' => route('article-category.index', $customer),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('article-category.store', $customer) }}">
                {{ csrf_field() }}

                @include('admin.article_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection