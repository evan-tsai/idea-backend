@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '文章類別',
    ]])

    <table-component :fields="{{ $fields }}"
                     base-url="{{ route('article-category.index', $customer) }}"
                     api-url="{{ route('api.admin.article-category.get', $customer) }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection