@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '文章類別' => route('article-category.index', $customer),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('article-category.update', [$customer, $articleCategory]) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('admin.article_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection