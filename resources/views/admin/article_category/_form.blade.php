<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
                class="form-control @formValidClass('name')"
                required
                value="{{ old('name', $articleCategory->name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">類型 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="type"
               class="form-control @formValidClass('type')"
               required
               value="{{ old('type', $articleCategory->type) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'type'])
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label">圖示</label>
    <div class="col-sm-10">
        <font-awesome-select icon="{{ old('icon', $articleCategory->icon) }}"></font-awesome-select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">排序</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $articleCategory->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>

<div class="card">
    <div class="card-header">
        設定
    </div>

    <div class="card-body">
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="#a-field"
                       id="a-field-tab"
                       class="nav-link active"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="a-field"
                       aria-selected="true">欄位</a>
                </li>
                <li class="nav-item">
                    <a href="#a-image"
                       id="a-image-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="a-image"
                       aria-selected="true">代表圖</a>
                </li>
                <li class="nav-item">
                    <a href="#a-editor"
                       id="a-editor-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="a-editor"
                       aria-selected="true">編輯器</a>
                </li>
                <li class="nav-item">
                    <a href="#a-tag"
                       id="a-tag-tab"
                       class="nav-link"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="a-tag"
                       aria-selected="true">文章標籤</a>
                </li>
            </ul>

            <div class="tab-content mb-2" id="myTabContent">
                <div class="form-group row tab-pane active" id="a-field" role="tabpanel" aria-labelledby="a-field-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">作者</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['fields', 'author']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">上架日</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['fields', 'publish_start']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">下架日</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['fields', 'publish_end']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">排序</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['fields', 'sort']
                                ])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Seo</label>
                            <div class="col-sm-10 p-2">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['fields', 'seo']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="a-image" role="tabpanel" aria-labelledby="a-image-tab">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['image', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['image', 'width']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                @include('partials.form.setting-input', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['image', 'height']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="a-editor" role="tabpanel" aria-labelledby="a-editor-tab">
                    <div class="col-sm-12 m-2">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['editors', 'enable']
                                ])
                            </div>
                        </div>

                        <multi-labeler :labels="{{ setting('default.article.editors.labels', $customer->id, config('default.article_settings.article.editors.labels'), $articleCategory->type) }}"
                                        name="article[editors][labels]"
                        ></multi-labeler>
                    </div>
                </div>
                <div class="form-group row tab-pane" id="a-tag" role="tabpanel" aria-labelledby="a-tag-tab">
                    <div class="col-sm-12 m-2">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">啟動</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['tags', 'enable']
                                ])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">排序</label>
                            <div class="col-sm-10 p-2 pl-3">
                                @include('partials.form.setting-switch', [
                                    'setting' => 'article_settings',
                                    'name' => 'article',
                                    'type' => $articleCategory->type,
                                    'columns' => ['tags', 'sort']
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>