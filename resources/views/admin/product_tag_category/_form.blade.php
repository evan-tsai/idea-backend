<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
                class="form-control @formValidClass('name')"
                required
                value="{{ old('name', $productTagCategory->name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">類型 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="type"
               class="form-control @formValidClass('type')"
               required
               value="{{ old('type', $productTagCategory->type) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'type'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">多選 @requiredInput</label>
    <div class="col-sm-10">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="multiple" id="multiple1" value="1" checked>
            <label class="form-check-label" for="multiple1">是</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="multiple" id="multiple2" value="0" @if ($productTagCategory->multiple === 0) checked @endif>
            <label class="form-check-label" for="multiple2">否</label>
        </div>

        @include('partials.form.invalid-feedback', ['field' => 'multiple'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">排序</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $productTagCategory->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>