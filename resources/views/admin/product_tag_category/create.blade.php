@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品標籤管理' => route('product-tag-category.index', $customer),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product-tag-category.store', $customer) }}">
                {{ csrf_field() }}

                @include('admin.product_tag_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection