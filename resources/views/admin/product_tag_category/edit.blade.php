@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品標籤管理' => route('product-tag-category.index', $customer),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product-tag-category.update', [$customer, $productTagCategory]) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('admin.product_tag_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection