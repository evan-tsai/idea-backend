@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品標籤管理',
    ]])

    <table-component :fields="{{ $fields }}"
                     base-url="{{ route('product-tag-category.index', $customer) }}"
                     api-url="{{ route('api.admin.product-tag-category.get', $customer) }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection