{{-- manager --}}
<div class="modal mm-animated fadeIn is-active modal-manager__Inmodal" style="z-index:10015" v-if="showModal">
    <div class="modal-background" @click.prevent="hideInputModal()"></div>
    <div class="modal-content mm-animated fadeInDown">
        <div class="box">
            @include('MediaManager::_manager', [
                'restrict' => [
                    'path' => customer('domain') . '/images',
                    'uploadTypes' => ['image/*'],
                ],
                'modal' => true
            ])
        </div>
    </div>
    <button class="modal-close is-large is-hidden-touch" @click.prevent="hideInputModal()"></button>
</div>
