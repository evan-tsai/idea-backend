@php
    $dot = '';
    $bracket = '';
    foreach ($columns as $column) {
        $dot .= '.' . $column;
        $bracket .= '[' . $column . ']';
    }
@endphp

<label class="switch switch-pill switch-success m-0">
    <input type="hidden" name="{{ $name . $bracket }}" value="0" />
    <input type="checkbox"
           class="switch-input"
           name="{{ $name . $bracket }}"
           value="1"
           @if (setting($name . $dot, $customer->id, config('default.' . $setting . '.' . $name . $dot), $type ?? null)) checked @endif>
    <span class="switch-slider"></span>
</label>