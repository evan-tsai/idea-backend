@php
    $dot = '';
    $bracket = '';
    foreach ($columns as $column) {
        $dot .= '.' . $column;
        $bracket .= '[' . $column . ']';
    }
@endphp

<input type="text" class="form-control"
       name="{{ $name . $bracket }}"
       value="{{ setting($name . $dot, $customer->id, config('default.' . $setting . '.' . $name . $dot), $type ?? null) }}">
