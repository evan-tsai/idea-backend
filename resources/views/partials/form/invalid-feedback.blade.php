@if ($errors->has($field))
    <div class="invalid-feedback d-block {{ $class ?? '' }}">{{ $errors->first($field) }}</div>
@endif