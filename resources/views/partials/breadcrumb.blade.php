<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        @foreach ($items as $name => $url)
            @if (is_int($name))
                {{-- it means dont have url --}}
                <li class="breadcrumb-item active" aria-current="page">{{ $url }}</li>
            @else
                <li class="breadcrumb-item"><a href="{{ $url }}">{{ $name }}</a></li>
            @endif
        @endforeach
    </ol>
</nav>