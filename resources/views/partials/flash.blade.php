@push('scripts')
    @foreach (session('flash_notification', collect())->toArray() as $message)
    <script>notify("{{ $message['level'] }}", "{!! $message['message'] !!}");</script>
    @endforeach
@endpush

{{ session()->forget('flash_notification') }}