@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品列表',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     :categories="{{ $categories }}"
                     base-url="{{ route('product.index') }}"
                     api-url="{{ route('api.admin.product.get') }}"
                     api-token="{{ auth()->user()->api_token }}">
    </table-component>
@endsection