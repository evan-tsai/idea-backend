@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品列表' => route('product.index'),
        '編輯',
    ]])
    @include('partials.form.media-manager')

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product.update', $product) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.product._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection