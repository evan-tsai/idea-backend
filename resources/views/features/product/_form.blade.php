@push('head_scripts')
    <script src="//cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
@endpush

<div class="form-group row">
    <label class="col-sm-2 col-form-label">產品類別 @requiredInput</label>
    <div class="col-sm-10">
        <multi-level-select
                field="category_id"
                :parent-category-id="{{ old('category_id', $product->category_id) ?? 'null' }}"
                :data-tree="{{ $categories }}"
        ></multi-level-select>

        @include('partials.form.invalid-feedback', ['field' => 'category_id'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">自訂 Url @requiredInput</label>
    <div class="col-sm-10">
        <slug-widget class="form-control @formValidClass('slug')" old-slug="{{ old('slug', $product->slug) }}"></slug-widget>

        @include('partials.form.invalid-feedback', ['field' => 'slug'])
    </div>
</div>

@if (setting('product.fields.model'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">型號</label>
    <div class="col-sm-10">
        <input type="text" name="model_name"
               class="form-control @formValidClass('model_name')"
               value="{{ old('model_name', $product->model_name) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'model_name'])
    </div>
</div>
@endif

@if (setting('product.fields.manufacture'))
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">製造日</label>
        <div class="col-sm-10">
            <date-picker name="manufacture_date" start="{{ old('manufacture_date', $product->manufacture_date) }}"></date-picker>

            @include('partials.form.invalid-feedback', ['field' => 'manufacture_date'])
        </div>
    </div>
@endif

@if (setting('product.fields.stock'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">庫存</label>
    <div class="col-sm-10">
        <input type="number" name="stock"
               class="form-control @formValidClass('stock')"
               value="{{ old('stock', $product->stock) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'stock'])
    </div>
</div>
@endif

@if (setting('product.fields.sort'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Sort</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $product->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>
@endif

@if ($tagTypes->count())
    <div class="card">
        <div class="card-header">
            產品標籤
        </div>

        <div class="card-body">
            @foreach ($tagTypes as $tagType)
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">{{ $tagType->name }}</label>
                    <div class="col-sm-10">
                        <model-select
                                {{ !$tagType->multiple ? ':multiple=false': '' }}
                                :models="{{ $tagType->tags }}"
                                field="tagIds[{{ $tagType->id }}]"
                                :selected-id="{{ json_encode(old('tagIds.' . $tagType->id, $product->tags->where('category_id', $tagType->id)->pluck('id'))) }}"
                        ></model-select>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach (customer('languages') as $language)
            <li class="nav-item">
                <a href="#{{ $language->id }}"
                   id="{{ $language->id }}-tab"
                   class="nav-link {{ $loop->first ? 'active' : '' }}"
                   data-toggle="tab"
                   role="tab"
                   aria-controls="{{ $language->id }}"
                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content mb-2" id="myTabContent">
        @foreach (customer('languages') as $language)
            <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">名稱&nbsp;@if ($loop->first) @requiredInput @endif</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('names.' . $language->id)"
                               name="names[{{ $language->id }}]"
                               value="{{ old('names.' . $language->id, $product->translateColumn($language->id, 'name')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'names.' . $language->id])
                    </div>
                </div>

                @if (setting('product.fields.description'))
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Description</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('description.' . $language->id)"
                               name="description[{{ $language->id }}]"
                               value="{{ old('description.' . $language->id, $product->translateColumn($language->id, 'description')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'description.' . $language->id])
                    </div>
                </div>
                @endif

                @if (setting('product.fields.url'))
                    <div class="col-sm-12">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">連結</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('url.' . $language->id)"
                                   name="url[{{ $language->id }}]"
                                   value="{{ old('url.' . $language->id, $product->translateColumn($language->id, 'url')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'url.' . $language->id])
                        </div>
                    </div>
                @endif

                @if (setting('product.image.enable') || setting('product.icon.enable'))
                    <div class="col-sm-12">
                        <div class="card-deck my-4">
                            @if (setting('product.image.enable'))
                                <image-component name="image_path[{{ $language->id }}]"
                                                 old="{{ old('image_path.' . $language->id, $product->translateColumn($language->id, 'image_path')) }}"
                                                 title="代表圖"
                                                 width="{{ setting('product.image.width') }}"
                                                 height="{{ setting('product.image.height') }}">
                                </image-component>
                            @endif
                            @if (setting('product.icon.enable'))
                                <image-component name="icon_path[{{ $language->id }}]"
                                                 old="{{ old('icon_path.' . $language->id, $product->translateColumn($language->id, 'icon_path')) }}"
                                                 title="圖示"
                                                 width="{{ setting('product.icon.width') }}"
                                                 height="{{ setting('product.icon.height') }}">
                                </image-component>
                            @endif
                        </div>
                    </div>
                @endif

                <div class="col-sm-12">
                    <collection-panel
                            type="{{ \App\Models\Collection::TYPE_GALLERY }}"
                            title="輪播圖"
                            name="galleries[{{ $language->id }}]"
                            :config="{{ setting_array('product.gallery') }}"
                            :items="{{ old('galleries.' . $language->id, array_get($galleries, $language->id, collect())->values()) }}">
                    </collection-panel>
                </div>

                @if (setting('product.fields.seo'))
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Seo Title</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('seo_title.' . $language->id)"
                               name="seo_title[{{ $language->id }}]"
                               value="{{ old('seo_title.' . $language->id, $product->translateColumn($language->id, 'seo_title')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'seo_title.' . $language->id])
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Seo Description</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('seo_description.' . $language->id)"
                               name="seo_description[{{ $language->id }}]"
                               value="{{ old('seo_description.' . $language->id, $product->translateColumn($language->id, 'seo_description')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'seo_description.' . $language->id])
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Seo Keywords</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('seo_keyword.' . $language->id)"
                               name="seo_keyword[{{ $language->id }}]"
                               value="{{ old('seo_keyword.' . $language->id, $product->translateColumn($language->id, 'seo_keyword')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'seo_keyword.' . $language->id])
                    </div>
                </div>
                @endif

                @if (setting('product.editors.enable'))
                <div class="col-sm-12">
                    <editor-panel :labels="{{ setting('product.editors.labels') }}"
                                  name="editors[{{ $language->id }}]"
                                  :items="{{ editorJsonContentEncode(old('editors.' . $language->id, optional($product->translateColumn($language->id, 'editors'))->pluck('content'))) }}">
                    </editor-panel>
                </div>
                @endif
            </div>
        @endforeach
    </div>
</div>
