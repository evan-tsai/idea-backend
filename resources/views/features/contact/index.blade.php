@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '聯絡資訊',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('customer-contact.save') }}">
                {{ csrf_field() }}
                @if (count($socials) > 0)
                    <div class="card">
                        <div class="card-header">
                            社交媒體
                        </div>

                        <div class="card-body">
                            @foreach ($socials as $social)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ $social->type }}</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="socials[{{ $social->type }}]"
                                               class="form-control"
                                               value="{{ old('socials.' . $social->type, $social->value) }}" />
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        聯絡資訊
                    </div>

                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            @foreach (customer('languages') as $language)
                                <li class="nav-item">
                                    <a href="#{{ $language->id }}"
                                       id="{{ $language->id }}-tab"
                                       class="nav-link {{ $loop->first ? 'active' : '' }}"
                                       data-toggle="tab"
                                       role="tab"
                                       aria-controls="{{ $language->id }}"
                                       aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content mb-2" id="myTabContent">
                            @foreach (customer('languages') as $language)
                                <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                                    @if (setting('contact.fields.phone'))
                                        <div class="col-sm-12">
                                            <div class="input-group mb-1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-box-label" style="width: 10em;">電話</span>
                                                </div>
                                                <input type="text" class="form-control @formValidClass('phone.' . $language->id)"
                                                       name="phone[{{ $language->id }}]"
                                                       value="{{ old('phone.' . $language->id, $contactInfo->translateColumn($language->id, 'phone')) }}">

                                                @include('partials.form.invalid-feedback', ['field' => 'phone.' . $language->id])
                                            </div>
                                        </div>
                                    @endif
                                    @if (setting('contact.fields.phone2'))
                                        <div class="col-sm-12">
                                            <div class="input-group mb-1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-box-label" style="width: 10em;">電話2</span>
                                                </div>
                                                <input type="text" class="form-control @formValidClass('phone2.' . $language->id)"
                                                       name="phone2[{{ $language->id }}]"
                                                       value="{{ old('phone2.' . $language->id, $contactInfo->translateColumn($language->id, 'phone2')) }}">

                                                @include('partials.form.invalid-feedback', ['field' => 'phone2.' . $language->id])
                                            </div>
                                        </div>
                                    @endif
                                    @if (setting('contact.fields.fax'))
                                        <div class="col-sm-12">
                                            <div class="input-group mb-1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-box-label" style="width: 10em;">傳真</span>
                                                </div>
                                                <input type="text" class="form-control @formValidClass('fax.' . $language->id)"
                                                       name="fax[{{ $language->id }}]"
                                                       value="{{ old('fax.' . $language->id, $contactInfo->translateColumn($language->id, 'fax')) }}">

                                                @include('partials.form.invalid-feedback', ['field' => 'fax.' . $language->id])
                                            </div>
                                        </div>
                                    @endif
                                    @if (setting('contact.fields.fax2'))
                                        <div class="col-sm-12">
                                            <div class="input-group mb-1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-box-label" style="width: 10em;">傳真2</span>
                                                </div>
                                                <input type="text" class="form-control @formValidClass('fax2.' . $language->id)"
                                                       name="fax2[{{ $language->id }}]"
                                                       value="{{ old('fax2.' . $language->id, $contactInfo->translateColumn($language->id, 'fax2')) }}">

                                                @include('partials.form.invalid-feedback', ['field' => 'fax2.' . $language->id])
                                            </div>
                                        </div>
                                    @endif
                                    @if (setting('contact.fields.email'))
                                        <div class="col-sm-12">
                                            <div class="input-group mb-1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-box-label" style="width: 10em;">電子信箱</span>
                                                </div>
                                                <input type="text" class="form-control @formValidClass('email.' . $language->id)"
                                                       name="email[{{ $language->id }}]"
                                                       value="{{ old('email.' . $language->id, $contactInfo->translateColumn($language->id, 'email')) }}">

                                                @include('partials.form.invalid-feedback', ['field' => 'email.' . $language->id])
                                            </div>
                                        </div>
                                    @endif
                                    @if (setting('contact.fields.address'))
                                        <div class="col-sm-12">
                                            <div class="input-group mb-1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input-box-label" style="width: 10em;">地址</span>
                                                </div>
                                                <input type="text" class="form-control @formValidClass('address.' . $language->id)"
                                                       name="address[{{ $language->id }}]"
                                                       value="{{ old('address.' . $language->id, $contactInfo->translateColumn($language->id, 'address')) }}">

                                                @include('partials.form.invalid-feedback', ['field' => 'address.' . $language->id])
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                @if (setting('email.receivers.enable'))
                    <div class="card">
                        <div class="card-header">
                            Email 收件人
                        </div>

                        <div class="card-body">
                            <multi-labeler :labels="{{ setting('email.receivers.items') }}"
                                           name="email[receivers][items]"
                                           type="email"
                                           title="Email"
                            ></multi-labeler>
                        </div>
                    </div>
                @endif

                <div>
                    <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
                </div>
            </form>
        </div>
    </div>
@endsection