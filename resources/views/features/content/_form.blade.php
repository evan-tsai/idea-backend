@push('head_scripts')
    <script src="//cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
@endpush

<input type="hidden" name="content_type" value="{{ $contentType->type }}">

@if (setting('content.' . $contentType->type . '.fields.sort'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Sort</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $content->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>
@endif

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach (customer('languages') as $language)
            <li class="nav-item">
                <a href="#{{ $language->id }}"
                   id="{{ $language->id }}-tab"
                   class="nav-link {{ $loop->first ? 'active' : '' }}"
                   data-toggle="tab"
                   role="tab"
                   aria-controls="{{ $language->id }}"
                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content mb-2" id="myTabContent">
        @foreach (customer('languages') as $language)
            <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">名稱&nbsp;@if ($loop->first) @requiredInput @endif</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('names.' . $language->id)"
                               name="names[{{ $language->id }}]"
                               value="{{ old('names.' . $language->id, $content->translateColumn($language->id, 'name')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'names.' . $language->id])
                    </div>
                </div>

                @if (setting('content.' . $contentType->type . '.fields.description'))
                    <div class="col-sm-12">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">描述</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('description.' . $language->id)"
                                   name="description[{{ $language->id }}]"
                                   value="{{ old('description.' . $language->id, $content->translateColumn($language->id, 'description')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'description.' . $language->id])
                        </div>
                    </div>
                @endif

                @if (setting('content.' . $contentType->type . '.fields.url'))
                    <div class="col-sm-12">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">連結</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('url.' . $language->id)"
                                   name="url[{{ $language->id }}]"
                                   value="{{ old('url.' . $language->id, $content->translateColumn($language->id, 'url')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'url.' . $language->id])
                        </div>
                    </div>
                @endif

                @if (setting('content.' . $contentType->type . '.image.enable'))
                    <div class="col-sm-12">
                        <div class="card-deck my-4">
                            <image-component name="image_path[{{ $language->id }}]"
                                             old="{{ old('image_path.' . $language->id, $content->translateColumn($language->id, 'image_path')) }}"
                                             title="代表圖"
                                             width="{{ setting('content.' . $contentType->type . '.image.width') }}"
                                             height="{{ setting('content.' . $contentType->type . '.image.height') }}">
                            </image-component>
                        </div>
                    </div>
                @endif

                @if (setting('content.' . $contentType->type . '.editors.enable'))
                    <div class="col-sm-12">
                        <editor-panel :labels="{{ setting('content.' . $contentType->type . '.editors.labels') }}"
                                      name="editors[{{ $language->id }}]"
                                      :items="{{ editorJsonContentEncode(old('editors.' . $language->id, optional($content->translateColumn($language->id, 'editors'))->pluck('content'))) }}">
                        </editor-panel>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</div>
