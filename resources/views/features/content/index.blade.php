@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $contentType->name,
        '內容列表',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     type="{{ $contentType->id }}"
                     base-url="{{ route('content.index', $contentType->type) }}"
                     api-url="{{ route('api.admin.content.get') }}"
                     api-token="{{ auth()->user()->api_token }}">
    </table-component>
@endsection