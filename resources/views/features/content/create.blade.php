@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $contentType->name,
        '內容列表' => route('content.index', $contentType->type),
        '新增',
    ]])
    @include('partials.form.media-manager')

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('content.store') }}">
                {{ csrf_field() }}

                @include('features.content._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection