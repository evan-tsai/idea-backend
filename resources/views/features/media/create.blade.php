@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $mediaType->name,
        '媒體列表' => route('media.index', $mediaType->type),
        '新增',
    ]])
    @include('partials.form.media-manager')

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('media.store') }}">
                {{ csrf_field() }}

                @include('features.media._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection