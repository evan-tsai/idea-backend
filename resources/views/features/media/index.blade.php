@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $mediaType->name,
        '媒體列表',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     type="{{ $mediaType->id }}"
                     base-url="{{ route('media.index', $mediaType->type) }}"
                     api-url="{{ route('api.admin.media.get') }}"
                     api-token="{{ auth()->user()->api_token }}">
    </table-component>
@endsection