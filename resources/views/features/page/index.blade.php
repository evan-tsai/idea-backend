@extends('layouts.admin')

@push('head_scripts')
    <script src="//cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
@endpush

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $page->name,
    ]])
    @include('partials.form.media-manager')

    <div class="m-4">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            @foreach (customer('languages') as $language)
                <li class="nav-item">
                    <a href="#{{ $language->id }}"
                       id="{{ $language->id }}-tab"
                       class="nav-link {{ $loop->first ? 'active' : '' }}"
                       data-toggle="tab"
                       role="tab"
                       aria-controls="{{ $language->id }}"
                       aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
                </li>
            @endforeach
        </ul>
        <form method="POST" action="{{ route('customer-page.save', $page) }}">
            {{ csrf_field() }}
            <div class="tab-content mb-2" id="myTabContent">
                @foreach (customer('languages') as $language)
                    <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                        <div class="col-sm-12">
                            <collection-panel
                                    type="{{ \App\Models\Collection::TYPE_BANNER }}"
                                    title="橫幅"
                                    name="banners[{{ $language->id }}]"
                                    :config="{{ setting_array('page.' . $page->type . '.banners') }}"
                                    :items="{{ old('banners.' . $language->id, array_get($banners, $language->id, collect())->values()) }}">
                            </collection-panel>
                        </div>

                        <div class="col-sm-12">
                            <collection-panel
                                    type="{{ \App\Models\Collection::TYPE_GALLERY }}"
                                    title="相簿"
                                    name="galleries[{{ $language->id }}]"
                                    :config="{{ setting_array('page.' . $page->type . '.gallery') }}"
                                    :items="{{ old('galleries.' . $language->id, array_get($gallery, $language->id, collect())->values()) }}">
                            </collection-panel>
                        </div>

                        @if (setting('page.' . $page->type . '.fields.seo'))
                            <div class="col-sm-12">
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text input-box-label" style="width: 10em;">Seo Title</span></span>
                                    </div>
                                    <input type="text" class="form-control @formValidClass('seo_title.' . $language->id)"
                                           name="seo_title[{{ $language->id }}]"
                                           value="{{ old('seo_title.' . $language->id, $page->translateColumn($language->id, 'seo_title')) }}">

                                    @include('partials.form.invalid-feedback', ['field' => 'seo_title.' . $language->id])
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text input-box-label" style="width: 10em;">Seo Description</span></span>
                                    </div>
                                    <input type="text" class="form-control @formValidClass('seo_description.' . $language->id)"
                                           name="seo_description[{{ $language->id }}]"
                                           value="{{ old('seo_description.' . $language->id, $page->translateColumn($language->id, 'seo_description')) }}">

                                    @include('partials.form.invalid-feedback', ['field' => 'seo_description.' . $language->id])
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text input-box-label" style="width: 10em;">Seo Keywords</span></span>
                                    </div>
                                    <input type="text" class="form-control @formValidClass('seo_keyword.' . $language->id)"
                                           name="seo_keyword[{{ $language->id }}]"
                                           value="{{ old('seo_keyword.' . $language->id, $page->translateColumn($language->id, 'seo_keyword')) }}">

                                    @include('partials.form.invalid-feedback', ['field' => 'seo_keyword.' . $language->id])
                                </div>
                            </div>
                        @endif

                        @if (setting('page.' . $page->type . '.editors.enable'))
                            <div class="col-sm-12">
                                <editor-panel :labels="{{ setting('page.' . $page->type . '.editors.labels') }}"
                                              name="editors[{{ $language->id }}]"
                                              :items="{{ editorJsonContentEncode(old('editors.' . $language->id, optional($page->translateColumn($language->id, 'editors'))->pluck('content'))) }}">
                                </editor-panel>
                            </div>
                        @endif
                    </div>
                @endforeach
                <div class="m-3">
                    <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
                </div>
            </div>
        </form>
    </div>
@endsection