@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $articleCategory->name,
        '文章標籤',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     category="{{ $articleCategory->id }}"
                     base-url="{{ route('article-tag.index', $articleCategory->type) }}"
                     api-url="{{ route('api.admin.article-tag.get') }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection