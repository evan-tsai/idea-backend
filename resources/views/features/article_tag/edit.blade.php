@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $articleCategory->name,
        '文章標籤' => route('article-tag.index', $articleCategory->type),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('article-tag.update', $articleTag) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.article_tag._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection