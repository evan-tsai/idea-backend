@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $articleCategory->name,
        '文章標籤' => route('article-tag.index', $articleCategory->type),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('article-tag.store') }}">
                {{ csrf_field() }}

                @include('features.article_tag._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection