<input type="hidden" name="media_type" value="{{ $mediaType->type }}">

@if (setting('media.' . $mediaType->type . '.category.sort'))
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Sort</label>
        <div class="col-sm-10">
            <input type="number" name="sort"
                   class="form-control @formValidClass('sort')"
                   value="{{ old('sort', $mediaCategory->sort) }}" />

            @include('partials.form.invalid-feedback', ['field' => 'sort'])
        </div>
    </div>
@endif

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach (customer('languages') as $language)
            <li class="nav-item">
                <a href="#{{ $language->id }}"
                   id="{{ $language->id }}-tab"
                   class="nav-link {{ $loop->first ? 'active' : '' }}"
                   data-toggle="tab"
                   role="tab"
                   aria-controls="{{ $language->id }}"
                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content mb-2" id="myTabContent">
        @foreach (customer('languages') as $language)
            <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">名稱&nbsp;@if ($loop->first) @requiredInput @endif</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('names.' . $language->id)"
                               name="names[{{ $language->id }}]"
                               value="{{ old('names.' . $language->id, $mediaCategory->translateColumn($language->id, 'name')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'names.' . $language->id])
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
