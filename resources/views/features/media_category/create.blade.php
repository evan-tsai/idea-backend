@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $mediaType->name,
        '媒體類別' => route('media-category.index', $mediaType->type),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('media-category.store') }}">
                {{ csrf_field() }}

                @include('features.media_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection