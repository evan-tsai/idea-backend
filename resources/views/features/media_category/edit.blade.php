@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $mediaType->name,
        '媒體類別' => route('media-category.index', $mediaType->type),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('media-category.update', $mediaCategory) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.media_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection