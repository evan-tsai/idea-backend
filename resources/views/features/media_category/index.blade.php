@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $mediaType->name,
        '媒體類別',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     type="{{ $mediaType->id }}"
                     base-url="{{ route('media-category.index', $mediaType->type) }}"
                     api-url="{{ route('api.admin.media-category.get') }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection