@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品標籤' => route('product-tag.index'),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product-tag.update', $productTag) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.product_tag._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection