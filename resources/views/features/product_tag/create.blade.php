@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品標籤' => route('product-tag.index'),
        '新增',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product-tag.store') }}">
                {{ csrf_field() }}

                @include('features.product_tag._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection