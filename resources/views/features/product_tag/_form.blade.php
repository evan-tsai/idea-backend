<div class="form-group row">
    <label class="col-sm-2 col-form-label">標籤類別 @requiredInput</label>
    <div class="col-sm-10">
        <multi-level-select
                field="category_id"
                :parent-category-id="{{ old('category_id', $productTag->category_id) ?? 'null' }}"
                :data-tree="{{ $categories }}"
        ></multi-level-select>

        @include('partials.form.invalid-feedback', ['field' => 'category_id'])
    </div>
</div>

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach (customer('languages') as $language)
            <li class="nav-item">
                <a href="#{{ $language->id }}"
                   id="{{ $language->id }}-tab"
                   class="nav-link {{ $loop->first ? 'active' : '' }}"
                   data-toggle="tab"
                   role="tab"
                   aria-controls="{{ $language->id }}"
                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content mb-2" id="myTabContent">
        @foreach (customer('languages') as $language)
            <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">名稱&nbsp;@if ($loop->first) @requiredInput @endif</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('names.' . $language->id)"
                               name="names[{{ $language->id }}]"
                               value="{{ old('names.' . $language->id, $productTag->translateColumn($language->id, 'name')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'names.' . $language->id])
                    </div>
                </div>

                @if (setting('product_tag.fields.description'))
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Description</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('description.' . $language->id)"
                               name="description[{{ $language->id }}]"
                               value="{{ old('description.' . $language->id, $productTag->translateColumn($language->id, 'description')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'description.' . $language->id])
                    </div>
                </div>
                @endif

                @if (setting('product_tag.image.enable'))
                <div class="col-sm-12">
                    <div class="card-deck my-4">
                        <image-component name="image_path[{{ $language->id }}]"
                                         old="{{ old('image_path.' . $language->id, $productTag->translateColumn($language->id, 'image_path')) }}"
                                         title="代表圖"
                                         width="{{ setting('product_tag.icon.width') }}"
                                         height="{{ setting('product_tag.icon.height') }}">
                        </image-component>
                    </div>
                </div>
                @endif
            </div>
        @endforeach
    </div>
</div>
