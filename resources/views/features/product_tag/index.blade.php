@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品標籤',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     :categories="{{ $categories }}"
                     base-url="{{ route('product-tag.index') }}"
                     api-url="{{ route('api.admin.product-tag.get') }}"
                     api-token="{{ auth()->user()->api_token }}"
                     :can-copy="false">
    </table-component>
@endsection