@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '信箱',
    ]])

    <email-table :fields="{{ $fields }}"
                     base-url="{{ route('email.index') }}"
                     api-url="{{ route('api.admin.email.get') }}"
                     api-token="{{ auth()->user()->api_token }}">
    </email-table>
@endsection