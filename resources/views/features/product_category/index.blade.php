@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品類別',
    ]])

    <category-tree :languages="{{ customer('languages') }}"
                   base-url="{{ route('product-category.index') }}"
                   api-url="{{ route('api.admin.product-category.get') }}"
                   api-token="{{ auth()->user()->api_token }}">
    </category-tree>
@endsection