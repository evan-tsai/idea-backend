<div class="form-group row">
    <label class="col-sm-2 col-form-label">上層類別 @requiredInput</label>
    <div class="col-sm-10">
        <multi-level-select
                field="parent_id"
                :parent-category-id="{{ old('parent_id', $productCategory->parent_id) ?? 'null' }}"
                :category-id="{{ $productCategory->id ?? 'null' }}"
                :data-tree="{{ $tree }}"
                :append-root="true"
        ></multi-level-select>

        @include('partials.form.invalid-feedback', ['field' => 'parent_id'])
    </div>
</div>

@if (setting('product_category.fields.slug'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">自訂 Url</label>
    <div class="col-sm-10">
        <slug-widget class="form-control @formValidClass('slug')" old-slug="{{ old('slug', $productCategory->slug) }}"></slug-widget>

        @include('partials.form.invalid-feedback', ['field' => 'slug'])
    </div>
</div>
@endif

@if (setting('product_category.fields.sort'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Sort</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $productCategory->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>
@endif

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach (customer('languages') as $language)
            <li class="nav-item">
                <a href="#{{ $language->id }}"
                   id="{{ $language->id }}-tab"
                   class="nav-link {{ $loop->first ? 'active' : '' }}"
                   data-toggle="tab"
                   role="tab"
                   aria-controls="{{ $language->id }}"
                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content mb-2" id="myTabContent">
        @foreach (customer('languages') as $language)
            <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">名稱&nbsp;@if ($loop->first) @requiredInput @endif</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('names.' . $language->id)"
                               name="names[{{ $language->id }}]"
                               value="{{ old('names.' . $language->id, $productCategory->translateColumn($language->id, 'name')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'names.' . $language->id])
                    </div>
                </div>

                @if (setting('product_category.fields.title'))
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Title</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('title.' . $language->id)"
                               name="title[{{ $language->id }}]"
                               value="{{ old('title.' . $language->id, $productCategory->translateColumn($language->id, 'title')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'title.' . $language->id])
                    </div>
                </div>
                @endif

                @if (setting('product_category.fields.description'))
                <div class="col-sm-12">
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Description</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('description.' . $language->id)"
                               name="description[{{ $language->id }}]"
                               value="{{ old('description.' . $language->id, $productCategory->translateColumn($language->id, 'description')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'description.' . $language->id])
                    </div>
                </div>
                @endif

                @if (setting('product_category.image.enable') || setting('product_category.icon.enable'))
                <div class="col-sm-12">
                    <div class="card-deck mb-4">
                        @if (setting('product_category.image.enable'))
                        <image-component name="image_path[{{ $language->id }}]"
                                         old="{{ old('image_path.' . $language->id, $productCategory->translateColumn($language->id, 'image_path')) }}"
                                         title="代表圖"
                                         width="{{ setting('product_category.image.width') }}"
                                         height="{{ setting('product_category.image.height') }}">
                        </image-component>
                        @endif
                        @if (setting('product_category.icon.enable'))
                        <image-component name="icon_path[{{ $language->id }}]"
                                         old="{{ old('icon_path.' . $language->id, $productCategory->translateColumn($language->id, 'icon_path')) }}"
                                         title="圖示"
                                         width="{{ setting('product_category.icon.width') }}"
                                         height="{{ setting('product_category.icon.height') }}">
                        </image-component>
                        @endif
                    </div>
                </div>
                @endif

                @if (setting('product_category.fields.seo'))
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Seo Title</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('seo_title.' . $language->id)"
                               name="seo_title[{{ $language->id }}]"
                               value="{{ old('seo_title.' . $language->id, $productCategory->translateColumn($language->id, 'seo_title')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'seo_title.' . $language->id])
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Seo Description</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('seo_description.' . $language->id)"
                               name="seo_description[{{ $language->id }}]"
                               value="{{ old('seo_description.' . $language->id, $productCategory->translateColumn($language->id, 'seo_description')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'seo_description.' . $language->id])
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">Seo Keywords</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('seo_keyword.' . $language->id)"
                               name="seo_keyword[{{ $language->id }}]"
                               value="{{ old('seo_keyword.' . $language->id, $productCategory->translateColumn($language->id, 'seo_keyword')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'seo_keyword.' . $language->id])
                    </div>
                </div>
                @endif
            </div>
        @endforeach
    </div>
</div>
