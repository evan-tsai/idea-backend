@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '產品類別' => route('product-category.index'),
        '編輯',
    ]])
    @include('partials.form.media-manager')

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('product-category.update', $productCategory) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.product_category._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection