@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        '使用者' => route('user.index'),
        '編輯',
    ]])

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('user.update', $user) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.user._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection