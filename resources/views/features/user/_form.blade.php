<div class="form-group row">
    <label class="col-sm-2 col-form-label">名稱 @requiredInput</label>
    <div class="col-sm-10">
        <input type="text" name="name"
                class="form-control @formValidClass('name')"
                value="{{ old('name', $user->name) }}"
                {{ $user->id ? 'readonly' : '' }} />

        @include('partials.form.invalid-feedback', ['field' => 'name'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Email @requiredInput</label>
    <div class="col-sm-10">
        <input type="email" name="email"
                class="form-control @formValidClass('email')"
                required
                value="{{ old('email', $user->email) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'email'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">密碼 @if(!$user->exists) @requiredInput @endif</label>
    <div class="col-sm-10">
        <input type="password" name="password"
                class="form-control @formValidClass('password')"
                {{ !$user->exists ? 'required' : '' }} />

        @include('partials.form.invalid-feedback', ['field' => 'password'])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">密碼確認 @if(!$user->exists) @requiredInput @endif</label>
    <div class="col-sm-10">
        <input type="password" name="password_confirmation"
                class="form-control @formValidClass('password_confirmation')"
                {{ !$user->exists ? 'required' : '' }} />

        @include('partials.form.invalid-feedback', ['field' => 'password_confirmation'])
    </div>
</div>