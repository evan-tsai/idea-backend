@push('head_scripts')
    <script src="//cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
@endpush

<input type="hidden" name="article_type" value="{{ $articleCategory->type }}">

<div class="form-group row">
    <label class="col-sm-2 col-form-label">自訂 Url @requiredInput</label>
    <div class="col-sm-10">
        <slug-widget class="form-control @formValidClass('slug')" old-slug="{{ old('slug', $article->slug) }}"></slug-widget>

        @include('partials.form.invalid-feedback', ['field' => 'slug'])
    </div>
</div>

@if (setting('article.' . $articleCategory->type . '.tags.enable'))
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">文章標籤</label>
        <div class="col-sm-10">
            <model-select
                    :multiple="true"
                    :models="{{ $tags }}"
                    field="tagIds"
                    :selected-id="{{ json_encode(old('tagIds', $article->tags->pluck('id'))) }}"
            ></model-select>
        </div>
    </div>
@endif

@if (setting('article.' . $articleCategory->type . '.fields.publish_start'))
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">上架日</label>
        <div class="col-sm-10">
            <date-picker name="publish_start" start="{{ old('publish_start', $article->publish_start ?: now()) }}"></date-picker>

            @include('partials.form.invalid-feedback', ['field' => 'publish_start'])
        </div>
    </div>
@endif

@if (setting('article.' . $articleCategory->type . '.fields.publish_end'))
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">下架日</label>
        <div class="col-sm-10">
            <date-picker name="publish_end" start="{{ old('publish_end', $article->publish_end) }}"></date-picker>

            @include('partials.form.invalid-feedback', ['field' => 'publish_end'])
        </div>
    </div>
@endif

@if (setting('article.' . $articleCategory->type . '.fields.sort'))
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Sort</label>
    <div class="col-sm-10">
        <input type="number" name="sort"
               class="form-control @formValidClass('sort')"
               value="{{ old('sort', $article->sort) }}" />

        @include('partials.form.invalid-feedback', ['field' => 'sort'])
    </div>
</div>
@endif

<div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach (customer('languages') as $language)
            <li class="nav-item">
                <a href="#{{ $language->id }}"
                   id="{{ $language->id }}-tab"
                   class="nav-link {{ $loop->first ? 'active' : '' }}"
                   data-toggle="tab"
                   role="tab"
                   aria-controls="{{ $language->id }}"
                   aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content mb-2" id="myTabContent">
        @foreach (customer('languages') as $language)
            <div class="form-group row tab-pane fade {{ $loop->first ? 'active show' : '' }}" id="{{ $language->id }}" role="tabpanel" aria-labelledby="{{ $language->id }}-tab">
                <div class="col-sm-12">
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text input-box-label" style="width: 10em;">標題&nbsp;@if ($loop->first) @requiredInput @endif</span>
                        </div>
                        <input type="text" class="form-control @formValidClass('names.' . $language->id)"
                               name="names[{{ $language->id }}]"
                               value="{{ old('names.' . $language->id, $article->translateColumn($language->id, 'name')) }}">

                        @include('partials.form.invalid-feedback', ['field' => 'names.' . $language->id])
                    </div>
                </div>
                @if (setting('article.' . $articleCategory->type . '.fields.author'))
                    <div class="col-sm-12">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">作者</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('author.' . $language->id)"
                                   name="author[{{ $language->id }}]"
                                   value="{{ old('author.' . $language->id, $article->translateColumn($language->id, 'author')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'author.' . $language->id])
                        </div>
                    </div>
                @endif

                @if (setting('article.' . $articleCategory->type . '.image.enable'))
                    <div class="col-sm-12">
                        <div class="card-deck mb-4">
                            <image-component name="image_path[{{ $language->id }}]"
                                             old="{{ old('image_path.' . $language->id, $article->translateColumn($language->id, 'image_path')) }}"
                                             title="代表圖"
                                             width="{{ setting('article.' . $articleCategory->type . '.image.width') }}"
                                             height="{{ setting('article.' . $articleCategory->type . '.image.height') }}">
                            </image-component>
                        </div>
                    </div>
                @endif

                @if (setting('article.' . $articleCategory->type . '.fields.seo'))
                    <div class="col-sm-12">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">Seo Title</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('seo_title.' . $language->id)"
                                   name="seo_title[{{ $language->id }}]"
                                   value="{{ old('seo_title.' . $language->id, $article->translateColumn($language->id, 'seo_title')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'seo_title.' . $language->id])
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">Seo Description</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('seo_description.' . $language->id)"
                                   name="seo_description[{{ $language->id }}]"
                                   value="{{ old('seo_description.' . $language->id, $article->translateColumn($language->id, 'seo_description')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'seo_description.' . $language->id])
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-box-label" style="width: 10em;">Seo Keywords</span>
                            </div>
                            <input type="text" class="form-control @formValidClass('seo_keyword.' . $language->id)"
                                   name="seo_keyword[{{ $language->id }}]"
                                   value="{{ old('seo_keyword.' . $language->id, $article->translateColumn($language->id, 'seo_keyword')) }}">

                            @include('partials.form.invalid-feedback', ['field' => 'seo_keyword.' . $language->id])
                        </div>
                    </div>
                @endif

                @if (setting('article.' . $articleCategory->type . '.editors.enable'))
                    <div class="col-sm-12">
                        <editor-panel :labels="{{ setting('article.' . $articleCategory->type . '.editors.labels') }}"
                                      name="editors[{{ $language->id }}]"
                                      :items="{{ editorJsonContentEncode(old('editors.' . $language->id, optional($article->translateColumn($language->id, 'editors'))->pluck('content'))) }}">
                        </editor-panel>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</div>
