@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $articleCategory->name,
        '文章列表',
    ]])

    <table-component :languages="{{ customer('languages') }}"
                     :fields="{{ $fields }}"
                     category="{{ $articleCategory->id }}"
                     base-url="{{ route('article.index', $articleCategory->type) }}"
                     api-url="{{ route('api.admin.article.get') }}"
                     api-token="{{ auth()->user()->api_token }}">
    </table-component>
@endsection