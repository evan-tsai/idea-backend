@extends('layouts.admin')

@section('content')
    @include('partials.breadcrumb', ['items' => [
        $articleCategory->name,
        '文章列表' => route('article.index', $articleCategory->type),
        '編輯',
    ]])
    @include('partials.form.media-manager')

    <div class="card m-4">
        <div class="card-body">
            <form method="POST" action="{{ route('article.update', $article) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @include('features.article._form')

                <button type="submit" class="btn btn-lg btn-primary btn-block">儲存</button>
            </form>
        </div>
    </div>
@endsection