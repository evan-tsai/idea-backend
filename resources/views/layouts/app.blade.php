<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.common._head')

<body>
<div id="app">
    @yield('content')
</div>

@include('layouts.common._foot')
</body>
</html>