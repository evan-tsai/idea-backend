<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.common._head')

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
@include('layouts.admin._header')

<div class="app-body" id="app">
    @if(isAdminDomain())
        @include('layouts.admin._super_admin_sidebar')
    @else
        @include('layouts.admin._sidebar')
    @endif

    <main class="main">
        @include('partials.flash')

        @yield('content')
    </main>

    <notifications class="notify-bottom-right" position="bottom right" style="z-index: 10020"></notifications>
</div>

<footer class="app-footer mt-4">
    <div class="mx-auto">
        Copyright © 2019 <a href="https://idea-infinite.com/" target="_blank">無極限</a> Information Marketing Co.Ltd. All rights reserved
    </div>
</footer>

@include('layouts.common._foot')
</body>
</html>
