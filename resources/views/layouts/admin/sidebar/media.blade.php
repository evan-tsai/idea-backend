@foreach ($mediaTypes as $mediaType)
    <li class="nav-item nav-dropdown {{ active(['media-item/' . $mediaType->type . '/*', 'media-category/' . $mediaType->type . '/*'], 'open') }}">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa {{ $mediaType->icon }}"></i> {{ $mediaType->name }}
        </a>
        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link {{ active(['media-category/' . $mediaType->type . '/*']) }}" href="{{ route('media-category.index', $mediaType->type) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 媒體類別
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link {{ active(['media-item/' . $mediaType->type . '/*']) }}" href="{{ route('media.index', $mediaType->type) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 媒體列表
                </a>
            </li>
        </ul>
    </li>
@endforeach