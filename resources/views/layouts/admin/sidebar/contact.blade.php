<li class="nav-item">
    <a class="nav-link {{ active('contact/*') }}" href="{{ route('customer-contact.index') }}">
        <i class="nav-icon fa fa-address-card"></i> 聯絡資訊
    </a>
</li>