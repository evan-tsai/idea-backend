@if (setting('email.enable'))
    <li class="nav-item">
        <a class="nav-link {{ active('email') }}" href="{{ route('email.index') }}">
            <i class="nav-icon fa fa-envelope"></i> 信箱 @if($mailCount) <span class="badge badge-primary">{{ $mailCount }}</span> @endif
        </a>
    </li>
@endif