@if (count($pages) > 0)
    <li class="nav-item nav-dropdown {{ active(['page/*'], 'open') }}">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-file-text"></i> 頁面管理
        </a>
        @foreach ($pages as $page)
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link {{ active(['page/' . $page->type . '/*']) }}" href="{{ route('customer-page.index', $page->type) }}">
                        <i class="nav-icon fa fa-fw ml-3"></i> {{ $page->name }}
                    </a>
                </li>
            </ul>
        @endforeach
    </li>
@endif
