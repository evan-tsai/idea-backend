@if (count($contents) > 0)
    <li class="nav-item nav-dropdown {{ active(['content/*'], 'open') }}">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-columns"></i> 內容管理
        </a>
        @foreach ($contents as $content)
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link {{ active(['content/' . $content->type . '/*']) }}" href="{{ route('content.index', $content->type) }}">
                        <i class="nav-icon fa fa-fw ml-3"></i> {{ $content->name }}
                    </a>
                </li>
            </ul>
        @endforeach
    </li>
@endif
