@if (setting('product.enable'))
    <li class="nav-item nav-dropdown {{ active(['product-category/*', 'product-tag/*', 'product/*'], 'open') }}">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-cogs"></i> 產品管理
        </a>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link {{ active('product-category/*') }}" href="{{ route('product-category.index') }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 產品類別
                </a>
            </li>
            @if (setting('product_tag.enable'))
            <li class="nav-item">
                <a class="nav-link {{ active('product-tag/*') }}" href="{{ route('product-tag.index') }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 產品標籤
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link {{ active('product/*') }}" href="{{ route('product.index') }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 產品列表
                </a>
            </li>
        </ul>
    </li>
@endif