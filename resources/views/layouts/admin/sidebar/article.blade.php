@foreach ($articleCategories as $articleCategory)
    <li class="nav-item nav-dropdown {{ active(['article/' . $articleCategory->type . '/*', 'article-tag/' . $articleCategory->type . '/*'], 'open') }}">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa {{ $articleCategory->icon }}"></i> {{ $articleCategory->name }}
        </a>
        @if (setting('article.' . $articleCategory->type . '.tags.enable'))
        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link {{ active(['article-tag/' . $articleCategory->type . '/*']) }}" href="{{ route('article-tag.index', $articleCategory->type) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 文章標籤
                </a>
            </li>
        </ul>
        @endif

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link {{ active(['article/' . $articleCategory->type . '/*']) }}" href="{{ route('article.index', $articleCategory->type) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 文章列表
                </a>
            </li>
        </ul>
    </li>
@endforeach