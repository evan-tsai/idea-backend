@foreach ($customerGroup as $prefix => $customers)
<li class="nav-title border-center pb-0" data-toggle="collapse" data-target="#collapse{{ $prefix }}">
    <span class="">{{ $prefix }}</span>
</li>
<div class="collapse  {{ $customers['active'] ? 'show' : '' }}" id="collapse{{ $prefix }}">
    @foreach ($customers['items'] as $customer)
    <li class="nav-item nav-dropdown {{ active('customer/'. $customer->id . '/*', 'open') }}">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon fa fa-users"></i> {{ $customer->domain }}
        </a>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('page.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 頁面產生
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('product-setting.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 產品設定
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('product-tag-category.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 產品標籤類別
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('article-category.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 文章類別
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('media-type.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 媒體類別
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('content-type.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 內容產生
                </a>
            </li>
        </ul>

        <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('contact.index', $customer) }}">
                    <i class="nav-icon fa fa-fw ml-3"></i> 聯絡設定
                </a>
            </li>
        </ul>
    </li>
    @endforeach
</div>
@endforeach