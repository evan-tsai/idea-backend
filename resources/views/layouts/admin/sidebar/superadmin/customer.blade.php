<li class="nav-item">
    <a class="nav-link {{ active(['customer.index', 'customer.create', 'customer.edit']) }}" href="{{ route('customer.index') }}">
        <i class="nav-icon fa fa-list"></i> Customers
    </a>
</li>