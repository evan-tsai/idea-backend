<li class="nav-item">
    <a class="nav-link {{ active('user/*') }}" href="{{ route('user.index') }}">
        <i class="nav-icon fa fa-user"></i> Users
    </a>
</li>