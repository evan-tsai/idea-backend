<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link {{ active('home') }}" href="{{ route('home') }}">
                    <i class="nav-icon fa fa-tachometer"></i> Dashboard
                </a>
            </li>
            @include('layouts.admin.sidebar.user')
            @include('layouts.admin.sidebar.page')
            @include('layouts.admin.sidebar.product')
            @include('layouts.admin.sidebar.article')
            @include('layouts.admin.sidebar.media')
            @include('layouts.admin.sidebar.content')
            @include('layouts.admin.sidebar.contact')
            @include('layouts.admin.sidebar.email')
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>