<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link {{ active('home') }}" href="{{ route('home') }}">
                    <i class="nav-icon fa fa-tachometer"></i> Dashboard
                </a>
            </li>
            @include('layouts.admin.sidebar.user')
            @include('layouts.admin.sidebar.superadmin.customer')
            @include('layouts.admin.sidebar.superadmin.customer_list')
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>