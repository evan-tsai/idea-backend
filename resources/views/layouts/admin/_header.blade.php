<header class="app-header navbar navbar-dark bg-dark">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="/">
        <img src="{{ asset('images/logo.svg') }}" style="height: 40%; margin-right: .5em;">
    </a>

    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown mx-2">
            <a class="nav-link d-flex align-items-center" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user-circle-o" aria-hidden="true" style="font-size: 1.5em;"></i>
                <span class="d-none d-md-inline ml-1">{{ auth()->user()->name }}</span>
                <i class="fa fa-angle-down ml-1" aria-hidden="true" style="font-size: 1.5em;"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right border border-secondary">
                <div class="dropdown-header text-center bg-secondary">
                    <strong>Hi, {{ auth()->user()->name }}</strong>
                </div>
                <a class="dropdown-item" href="{{ route('logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> 登出
                </a>
            </div>
        </li>
    </ul>
</header>