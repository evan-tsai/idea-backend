<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ $title }}</title>

    <style>
        @import url(https://fonts.googleapis.com/earlyaccess/notosanstc.css);


        * {
            margin: 0;
            font-family: 'Noto Sans TC', sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6em;
        }

        table td {
            vertical-align: top;
        }

        .body-wrap {
            width: 100%;
        }

        .email-container {
            display: block !important;
            max-width: 600px !important;
            width: 100% !important;
            clear: both !important;
        }

        .email-main {
            background-color: white;
        }

        .head {
            width: 100%;
            text-align: left;
            padding: 10px;
            font-size: 20px;
        }

        .type,
        .value {
            padding: 15px 10px;
            font-size: 18px;
        }

        .type {
            width: 20%;
            min-width: 150px;
        }

        .value {
            width: 80%;
        }

        @media only screen and (max-width: 640px) {
            .email-container {
                padding: 0 !important;
                width: 100% !important;
            }
        }
    </style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage">
<div class="email-container">
    <table class="email-main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope itemtype="http://schema.org/ConfirmAction">
        <tr>
            <td class="content-wrap">
                <meta itemprop="name" content="Confirm Email" />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <th class="head" colspan="5">{{ $title }}</th>
                    </tr>
                    @foreach ($data as $name => $value)
                        @if (!empty($value))
                        <tr>
                            <td class="type">{{ ucfirst(str_replace('_', ' ', $name)) }}</td>
                            <td class="value" colspan="4">{{ is_array($value) ? implode(', ', $value) : $value }}</td>
                        </tr>
                        @endif
                    @endforeach
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>