@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}" />
    <style>
        .login-box {
            width: 600px;
            min-height: 280px;
            border: 1px solid #ccc;
            margin: 0 auto;
            margin-bottom: 70px;
            background: rgba(255, 255, 255, 0.6);
            border: 1px solid #ccc;
            position: absolute;
            transform: translate3d(-50%, -50%, 0);
            top: 50%;
            left: 50%;
        }
        .box-form {
            margin: 50px auto;
            padding: 0 30px;
        }
        .text-muted {
            color: #fff !important;
            font-weight: normal;
            font-size: 30px;
            line-height: 18px;
            text-align: center;
            text-transform: uppercase;
            position: absolute;
            transform: translate3d(-50%, -50%, 0);
            top: -50px;
            left: 50%;
            width: 100%;
        }
        .text-muted span {
            font-size: 12px;
            letter-spacing: 2px;
        }
        .input-group-text {
            width: 35px;
            background: #999;
            color: #fff;
            border: 1px solid #999;
        }
        .bg-dark {
            background: #333 !important;
        }
        .copyright {
            position: absolute;
            color: #ccc;
            left: 8%;
            bottom:-50px;
        }
        input:focus {
            border-style: solid;
            border-color: #fff !important;
            box-shadow: 0 0 15px #fff !important;
        }
        .form-captcha{
            background:transparent !important;
            border:none !important;
        }
        @media (max-width: 640px) {
            .login-box {
                width: 90%;
            }
        }
    </style>
@endpush

@push('scripts')
    <script src="{{ asset('js/login.js') }}"></script>
    <script src="{{ config('captcha.js_url') }}" async defer></script>
@endpush

@section('content')
    <div id="large-header" class="large-header">
        <canvas id="canvas"></canvas>

        <div class="login-box">
            <form
                    method="POST"
                    action="{{ route('login') }}"
                    class="box-form"
            >
                {{ csrf_field() }}
                <h1 class="text-muted">
                    {{ config('app.name') }}<br />
                    <span>Management system</span>
                </h1>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-user"></i>
              </span>
                    </div>
                    <input
                            type="text"
                            name="name"
                            class="form-control "
                            placeholder="登入帳號"
                            value="{{ old('name') }}"
                    />
                    @if ($errors->has('name'))
                        <div class="invalid-feedback text-light">{{ $errors->first('name') }}</div>
                    @endif
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-key"></i>
              </span>
                    </div>
                    <input
                            type="password"
                            name="password"
                            class="form-control "
                            placeholder="密碼"
                    />
                    @if ($errors->has('password'))
                        <div class="invalid-feedback text-light">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="form-group mb-3">
                    <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ config('captcha.site_key')  }}"></div>
                </div>

                <button
                        type="submit"
                        class="btn bg-dark btn-lg btn-block text-white px-4"
                >
                    登入
                </button>
            </form>
            <div class="copyright">
                Copyright © 2019 <a href="https://idea-infinite.com" target="_blank">無極限</a> Information Marketing Co.Ltd..All rights
                reserved
            </div>
        </div>
        <div id="stars"></div>
        <div id="stars2"></div>
        <div id="stars3"></div>
    </div>
@endsection