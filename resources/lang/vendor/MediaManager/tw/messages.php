<?php

return array (
  'add' => 
  array (
    'folder' => 'Add Folder',
    'new_folder' => 'Add New Folder',
    'url' => 'Url ...',
  ),
  'ajax_error' => '系統發生錯誤',
  'application' => 'Application',
  'are_you_sure_delete' => '確定刪除?',
  'audio' => 
  array (
    'album' => 'Album',
    'artist' => 'Artist',
    'main' => 'Audio',
    'support' => 'Your browser does not support the audio element',
    'title' => 'Title',
    'track' => 'Track Nº',
    'year' => 'Year',
  ),
  'cancel' => '取消',
  'clear' => 'Clear :attr',
  'close' => '關閉',
  'copy' => 
  array (
    'copied' => 'Copied',
    'file_folder' => 'Copy File/Folder',
    'files' => 'Copy Files Instead ?',
    'main' => 'Copy',
    'success' => 'Successfully Copied',
    'to_cp' => 'Copy Link To Clipboard',
  ),
  'create_success' => 'Successfully Created',
  'crop' => 
  array (
    'apply' => '確認',
    'flip_horizontal' => '水平翻轉',
    'flip_vertical' => '垂直翻轉',
    'main' => '裁切',
    'presets' => 'Presets',
    'reset' => '取消',
    'reset_filters' => 'Clear Filters',
    'rotate_left' => '向左旋轉',
    'rotate_right' => '向右旋轉',
    'zoom_in' => '放大',
    'zoom_out' => '縮小',
  ),
  'delete' => 
  array (
    'confirm' => '確定刪除',
    'folder' => 'Deleting a folder will also remove all of its content',
    'main' => '刪除',
    'success' => '刪除成功',
  ),
  'destination_folder' => 'Destination Folder',
  'dimension' => '尺寸',
  'download' => 
  array (
    'downloaded' => 'Downloaded',
    'file' => 'Download File',
    'folder' => 'Download Folder',
    'sep' => 'Folders Should Be Downloaded Separately',
  ),
  'editor' => 
  array (
    'diff' => 'Toggle Diff',
    'main' => '圖片編輯',
  ),
  'error' => 
  array (
    'already_exists' => '檔案名稱已存在',
    'altered_fwli' => 'Can\'t Be Moved, Renamed Or Deleted Because It Has Some Locked Items',
    'cant_upload' => 'Sorry File Can\'t Be Uploaded',
    'creating_dir' => 'Something Gone Wrong While Creating The Directory, Please Check Your Permissions',
    'deleting_file' => 'Something Gone Wrong While Deleting This File/Folder, Please Check Your Permissions,',
    'doesnt_exist' => 'Folder ":attr" Doesnt Exist',
    'moving' => 'There Seems To Be A Problem Moving This File/Folder, Make Sure You Have The Correct Permissions.',
    'moving_cloud' => 'Cloud Folders Can\'t Be "renamed, Moved Or Copied"',
  ),
  'filter' => 'Filter',
  'filter_by' => 'Filter By :attr',
  'find' => '搜尋...',
  'folder' => 'Folder',
  'found' => 'Found',
  'go_to_folder' => 'Go To Folder !',
  'image' => 'Image',
  'items' => 'Item(s)',
  'last_modified' => '最後修改',
  'library' => 'Home',
  'loading' => '載入圖片',
  'lock' => 'Lock Item',
  'lock_success' => '":attr" Lock Was Updated',
  'locked' => 'Locked',
  'move' => 
  array (
    'file_folder' => 'Move File/Folder',
    'main' => '移動',
    'success' => 'Successfully Moved',
  ),
  'name' => '名稱',
  'new' => 
  array (
    'create_folder' => 'Create New Folder',
    'create_folder_notif' => 'New Folder Was created',
    'file_folder' => '新名稱',
    'folder_name' => 'New Folder Name',
  ),
  'no_files_in_folder' => 'No Files In This Folder',
  'no_val' => 'Maybe You Should Add Something First !!!',
  'non' => '無',
  'not_allowed_file_ext' => 'Files Of Type ":attr" Are Not Allowed',
  'nothing_found' => 'Nothing Found',
  'open' => '開啟',
  'pdf' => 'Your Browser Does Not Support Pdfs. Please Download The Pdf To View It',
  'preview' => '預覽',
  'public_url' => '連結',
  'refresh_notif' => 'Press The Refresh Button To View Them',
  'rename' => 
  array (
    'file_folder' => '重新命名',
    'main' => '重新命名',
    'success' => '成功',
  ),
  'save' => 
  array (
    'link' => 'Upload Image From Url',
    'main' => 'Save',
    'success' => '存檔成功',
  ),
  'search' => 
  array (
    'glbl' => 'Global Search',
    'glbl_avail' => 'Global Search Is Now Available.',
    'main' => 'Search',
  ),
  'select' => 
  array (
    'all' => 'Select All',
    'bulk' => 'Bulk Select',
    'non' => 'Select Non',
    'nothing' => 'No File Or Folder Selected',
    'selected' => 'Selected',
  ),
  'size' => '檔案大小',
  'sort_by' => '排序方式',
  'stand_by' => '請稍等 ...',
  'text' => 'Text',
  'title' => 'Media Manager',
  'too_many_files' => 'Items Will Be Deleted !',
  'total' => '總共',
  'type' => '類別',
  'unlock' => 'UnLock Item',
  'upload' => 
  array (
    'main' => '上傳',
    'new_uploads_notif' => 'New Items Were Uploaded',
    'success' => '上傳成功',
    'text' => '拖放文件<br>或<br>點擊上傳',
    'use_random_names' => 'Use Random Names ?',
  ),
  'video' => 'Video',
  'video_support' => 'Your Browser Does Not Support The Video Tag',
  'visibility' => 
  array (
    'error' => '":attr" Visibility Couldn\'t Be Changed',
    'main' => 'Visibility',
    'set' => 'Change Visibility',
    'success' => '":attr" Visibility Was Updated',
  ),
);