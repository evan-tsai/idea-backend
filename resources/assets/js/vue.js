window.Vue = require('vue');

import Notifications from 'vue-notification';
Vue.use(Notifications);
window.notify = function (type, text) {
    // types: warn, error, success
    if (type === 'warning') {
        type = 'warn';
    }
    return Vue.notify({type, text});
};

require('../vendor/MediaManager/js/manager');

Vue.component('table-component', require('./components/Table/TableComponent.vue'));
Vue.component('email-table', require('./components/Table/EmailTable.vue'));
Vue.component('category-tree', require('./components/TreeList/CategoryTree.vue'));
Vue.component('model-select', require('./components/Select/ModelSelect.vue'));
Vue.component('multi-level-select', require('./components/Select/MultiLevelSelect.vue'));
Vue.component('slug-widget', require('./components/SlugWidget.vue'));
Vue.component('editor-panel', require('./components/Editor/EditorPanel.vue'));
Vue.component('multi-labeler', require('./components/MultiLabeler.vue'));
Vue.component('image-component', require('./components/MediaManager/ImageComponent.vue'));
Vue.component('collection-panel', require('./components/MediaManager/CollectionPanel.vue'));
Vue.component('admin-media-manager', require('./components/MediaManager/AdminMediaManager.vue'));
Vue.component('date-picker', require('./components/DatePicker.vue'));
Vue.component('font-awesome-select', require('./components/FontAwesomeSelect'));

const app = new Vue({
    el: '#app'
});