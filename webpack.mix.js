let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('resources/assets/vendor/MediaManager/sass/manager.scss', 'public/assets/vendor/MediaManager/style.css')
    .copyDirectory('resources/assets/vendor/MediaManager/dist', 'public/assets/vendor/MediaManager');

mix.scripts([
    'resources/assets/login/TweenLite.min.js',
    'resources/assets/login/EasePack.min.js',
    'resources/assets/login/plugs.js',
], 'public/js/login.js');

mix.styles([
    'resources/assets/login/app.css',
    'resources/assets/login/dynamic.css',
], 'public/css/login.css')
    .copy('resources/assets/login/images/system-bg.jpg', 'public/images/system-bg.jpg');