<?php

return [
    'product_category' => [
        'fields' => [
            'slug' => true,
            'sort' => true,
            'title' => true,
            'description' => true,
            'seo' => true,
        ],
        'image' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
        'icon' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
    ],

    'product' => [
        'enable' => true,
        'fields' => [
            'model' => true,
            'manufacture' => false,
            'stock' => true,
            'description' => true,
            'url' => true,
            'sort' => true,
            'seo' => true,
        ],
        'image' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
        'icon' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
        'gallery' => [
            'enable' => true,
            'max' => 10,
            'width' => null,
            'height' => null,
            'fields' => [
                'name' => false,
                'description' => false,
                'video_url' => false,
                'url' => false,
            ],
        ],
        'editors' => [
            'enable' => true,
            'labels' => '["內容"]',
        ],
    ],

    'product_tag' => [
        'enable' => true,
        'fields' => [
            'description' => true,
        ],
        'image' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
    ]
];