<?php

return [
    'media' => [
        'fields' => [
            'description' => true,
            'url' => true,
            'sort' => true,
        ],
        'image' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
        'category' => [
            'sort' => true,
        ]
    ]
];