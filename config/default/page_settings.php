<?php

return [
    'page' => [
        'fields' => [
            'seo' => true,
        ],
        'banners' => [
            'enable' => true,
            'max' => 5,
            'width' => null,
            'height' => null,
            'fields' => [
                'name' => false,
                'description' => false,
                'video_url' => false,
                'url' => false,
            ],
        ],
        'gallery' => [
            'enable' => false,
            'max' => 10,
            'width' => null,
            'height' => null,
            'fields' => [
                'name' => false,
                'description' => false,
                'video_url' => false,
                'url' => false,
            ],
        ],
        'editors' => [
            'enable' => true,
            'labels' => '["內容"]',
        ],
    ]
];