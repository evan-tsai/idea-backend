<?php

return [
    'contact' => [
        'fields' => [
            'phone' => true,
            'phone2' => false,
            'fax' => true,
            'fax2' => false,
            'email' => true,
            'address' => true,
        ],
    ],
    'email' => [
        'enable' => true,
        'receivers' => [
            'enable' => true,
            'items' => '[""]',
        ],
    ]
];