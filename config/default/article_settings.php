<?php

return [
    'article' => [
        'fields' => [
            'author' => false,
            'publish_start' => true,
            'publish_end' => true,
            'sort' => true,
            'seo' => true,
        ],
        'image' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
        'editors' => [
            'enable' => true,
            'labels' => '["內容"]',
        ],
        'tags' => [
            'enable' => true,
            'sort' => true,
        ],
    ]
];