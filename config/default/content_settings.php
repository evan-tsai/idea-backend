<?php

return [
    'content' => [
        'fields' => [
            'description' => false,
            'url' => false,
            'sort' => true,
        ],
        'image' => [
            'enable' => true,
            'width' => null,
            'height' => null,
        ],
        'editors' => [
            'enable' => true,
            'labels' => '["內容"]',
        ],
    ]
];