<?php

return [
    'site_key' => env('RECAPTCHA_SITEKEY', '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'),
    'secret' =>  env('RECAPTCHA_SECRET', '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'),
    'js_url' => env('RECAPTCHA_JS', 'https://www.recaptcha.net/recaptcha/api.js'),
    'verify_url' => env('RECAPTCHA_VERIFY', 'https://www.recaptcha.net/recaptcha/api/')
];