<?php

return [
    'username' => 'idea_admin',
    'domain' => 'idea',
    'email' => 'infinite17815@gmail.com',
    'password' => env('ADMIN_PASSWORD', 'secret'),
];