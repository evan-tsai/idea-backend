<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('frontend_api')->prefix('v1')->name('api.v1.')->group(function () {
    Route::namespace('v1')->group(base_path('routes/api/v1.php'));
});

Route::prefix('admin')->name('api.admin.')->group(function () {
    Route::middleware('api.admin')->namespace('Admin')->group(base_path('routes/api/admin.php'));
});