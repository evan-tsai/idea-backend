<?php

// Auth
Route::namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');
});

// Control Panel
Route::middleware('auth')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    ctf0\MediaManager\MediaRoutes::routes();

    Route::namespace('Features')->group(base_path('routes/features.php'));
    Route::namespace('Admin')->middleware('admin.domain')->group(base_path('routes/admin.php'));
});