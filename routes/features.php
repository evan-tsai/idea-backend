<?php

// User
Route::resource('user', 'UserController');

// Product Category
Route::resource('product-category', 'ProductCategoryController');
Route::get('product-category/{productCategory}/copy', 'ProductCategoryController@copy')->name('product-category.copy');

// Product
Route::resource('product', 'ProductController');
Route::get('product/{product}/copy', 'ProductController@copy')->name('product.copy');

// Product Tag
Route::resource('product-tag', 'ProductTagController');

// Articles
Route::get('article/{articleCategory}', 'ArticleController@index')->name('article.index');
Route::get('article/{articleCategory}/create', 'ArticleController@create')->name('article.create');
Route::get('article/{articleCategory}/{article}/edit', 'ArticleController@edit')->name('article.edit');
Route::get('article/{articleCategory}/{article}/copy', 'ArticleController@copy')->name('article.copy');
Route::post('article/store', 'ArticleController@store')->name('article.store');
Route::put('article/{article}', 'ArticleController@update')->name('article.update');
Route::delete('article/{articleCategory}/{article}', 'ArticleController@destroy')->name('article.delete');

// Article Tags
Route::get('article-tag/{articleCategory}', 'ArticleTagController@index')->name('article-tag.index');
Route::get('article-tag/{articleCategory}/create', 'ArticleTagController@create')->name('article-tag.create');
Route::get('article-tag/{articleCategory}/{article_tag}/edit', 'ArticleTagController@edit')->name('article-tag.edit');
Route::post('article-tag/store', 'ArticleTagController@store')->name('article-tag.store');
Route::put('article-tag/{article_tag}', 'ArticleTagController@update')->name('article-tag.update');
Route::delete('article-tag/{articleCategory}/{article_tag}', 'ArticleTagController@destroy')->name('article-tag.delete');

// Page
Route::get('page/{page}', 'PageController@index')->name('customer-page.index');
Route::post('page/{page}/save', 'PageController@save')->name('customer-page.save');

// Footer
Route::get('contact', 'ContactController@index')->name('customer-contact.index');
Route::post('contact/save', 'ContactController@save')->name('customer-contact.save');

// Email
Route::get('email', 'EmailController@index')->name('email.index');
Route::get('email/download', 'EmailController@download')->name('email.download');
Route::delete('email/{email}', 'EmailController@destroy')->name('email.delete');

// Media Category
Route::get('media-category/{mediaType}', 'MediaCategoryController@index')->name('media-category.index');
Route::get('media-category/{mediaType}/create', 'MediaCategoryController@create')->name('media-category.create');
Route::get('media-category/{mediaType}/{media_category}/edit', 'MediaCategoryController@edit')->name('media-category.edit');
Route::post('media-category/store', 'MediaCategoryController@store')->name('media-category.store');
Route::put('media-category/{media_category}', 'MediaCategoryController@update')->name('media-category.update');
Route::delete('media-category/{mediaType}/{media_category}', 'MediaCategoryController@destroy')->name('media-category.delete');

// Media
Route::get('media-item/{mediaType}', 'MediaItemController@index')->name('media.index');
Route::get('media-item/{mediaType}/create', 'MediaItemController@create')->name('media.create');
Route::get('media-item/{mediaType}/{media}/edit', 'MediaItemController@edit')->name('media.edit');
Route::get('media-item/{mediaType}/{media}/copy', 'MediaItemController@copy')->name('media.copy');
Route::post('media-item/store', 'MediaItemController@store')->name('media.store');
Route::put('media-item/{media}', 'MediaItemController@update')->name('media.update');
Route::delete('media-item/{mediaType}/{media}', 'MediaItemController@destroy')->name('media.delete');

// Content
Route::get('content/{contentType}', 'ContentController@index')->name('content.index');
Route::get('content/{contentType}/create', 'ContentController@create')->name('content.create');
Route::get('content/{contentType}/{content}/edit', 'ContentController@edit')->name('content.edit');
Route::get('content/{contentType}/{content}/copy', 'ContentController@copy')->name('content.copy');
Route::post('content/store', 'ContentController@store')->name('content.store');
Route::put('content/{content}', 'ContentController@update')->name('content.update');
Route::delete('content/{contentType}/{content}', 'ContentController@destroy')->name('content.delete');