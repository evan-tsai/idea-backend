<?php

Route::resource('customer', 'CustomerController');

Route::prefix('customer/{customer}')->group(function () {
    Route::resource('product-tag-category', 'ProductTagCategoryController');
    Route::resource('article-category', 'ArticleCategoryController');
    Route::resource('page', 'PageController');
    Route::resource('media-type', 'MediaTypeController');
    Route::resource('content-type', 'ContentTypeController');

    Route::get('product-setting', 'ProductSettingController@index')->name('product-setting.index');
    Route::post('product-setting', 'ProductSettingController@save')->name('product-setting.save');

    Route::get('contact', 'ContactController@index')->name('contact.index');
    Route::post('contact', 'ContactController@save')->name('contact.save');
});