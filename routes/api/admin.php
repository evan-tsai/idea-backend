<?php

Route::get('users', 'UserController@get')->name('users.get');
Route::get('customers', 'CustomerController@get')->name('customers.get');

Route::get('product', 'ProductController@get')->name('product.get');
Route::post('product/{id}/status', 'ProductController@status')->name('product.status');

Route::get('product-category', 'ProductCategoryController@get')->name('product-category.get');
Route::post('product-category/{id}/status', 'ProductCategoryController@status')->name('product-category.status');

Route::get('product-tag', 'ProductTagController@get')->name('product-tag.get');
Route::post('product-tag/{id}/status', 'ProductTagController@status')->name('product-tag.status');

Route::get('article', 'ArticleController@get')->name('article.get');
Route::post('article/{id}/status', 'ArticleController@status')->name('article.status');

Route::get('article-tag', 'ArticleTagController@get')->name('article-tag.get');
Route::post('article-tag/{id}/status', 'ArticleTagController@status')->name('article-tag.status');

Route::get('media-category', 'MediaCategoryController@get')->name('media-category.get');
Route::post('media-category/{id}/status', 'MediaCategoryController@status')->name('media-category.status');

Route::get('media', 'MediaController@get')->name('media.get');
Route::post('media/{id}/status', 'MediaController@status')->name('media.status');

Route::get('content', 'ContentController@get')->name('content.get');
Route::post('content/{id}/status', 'ContentController@status')->name('content.status');

Route::get('email', 'EmailController@get')->name('email.get');
Route::post('email/{id}/read', 'EmailController@read')->name('email.read');
Route::post('email/read-all', 'EmailController@readAll')->name('email.read-all');

Route::get('customer/{customer}/product-tag-category', 'ProductTagCategoryController@get')->name('product-tag-category.get');
Route::post('customer/{customer}/product-tag-category/{id}/status', 'ProductTagCategoryController@status')->name('product-tag-category.status');

Route::get('customer/{customer}/article-category', 'ArticleCategoryController@get')->name('article-category.get');
Route::post('customer/{customer}/article-category/{id}/status', 'ArticleCategoryController@status')->name('article-category.status');

Route::get('customer/{customer}/page', 'PageController@get')->name('page.get');
Route::post('customer/{customer}/page/{id}/status', 'PageController@status')->name('page.status');

Route::get('customer/{customer}/media-type', 'MediaTypeController@get')->name('media-type.get');
Route::post('customer/{customer}/media-type/{id}/status', 'MediaTypeController@status')->name('media-type.status');

Route::get('customer/{customer}/content-type', 'ContentTypeController@get')->name('content-type.get');
Route::post('customer/{customer}/content-type/{id}/status', 'ContentTypeController@status')->name('content-type.status');