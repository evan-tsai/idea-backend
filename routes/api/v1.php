<?php
Route::get('product-category', 'ProductCategoryController@get')->name('product-category.get');
Route::get('product', 'ProductController@get')->name('product.get');
Route::get('product-tag', 'ProductTagController@get')->name('product-tag.get');

Route::get('page', 'PageController@get')->name('page.get');

Route::get('contact', 'ContactController@get')->name('contact.get');

Route::get('social', 'SocialController@get')->name('social.get');

Route::get('article/{articleCategory}', 'ArticleController@get')->name('article.get');
Route::get('article-tag/{articleCategory}', 'ArticleTagController@get')->name('article-tag.get');

Route::get('media-category/{mediaType}', 'MediaCategoryController@get')->name('media-category.get');
Route::get('media/{mediaType}', 'MediaController@get')->name('media.get');

Route::get('content/{contentType}', 'ContentController@get')->name('content.get');

Route::post('email/send', 'EmailController@send')->name('email.send');