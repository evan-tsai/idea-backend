<?php

namespace App\Composers;

use App\Queries\ArticleCategoryQuery;
use App\Queries\ContentTypeQuery;
use App\Queries\EmailQuery;
use App\Queries\MediaTypeQuery;
use App\Queries\PageQuery;

class SidebarComposer
{
    public function compose($view)
    {
        $articleCategories = $this->getArticleCategories();
        $mediaTypes = $this->getMediaTypes();
        $pages = $this->getPages();
        $contents = $this->getContentTypes();
        $mailCount = $this->getMailCount();

        $view->with(compact('articleCategories', 'pages', 'mailCount', 'mediaTypes', 'contents'));
    }

    protected function getMediaTypes()
    {
        return resolve(MediaTypeQuery::class)->get();
    }

    protected function getContentTypes()
    {
        return resolve(ContentTypeQuery::class)->get();
    }

    protected function getArticleCategories()
    {
        return resolve(ArticleCategoryQuery::class)->get();
    }

    protected function getPages()
    {
        return resolve(PageQuery::class)->get();
    }

    protected function getMailCount()
    {
        $emails = resolve(EmailQuery::class)->get();
        return $emails->where('read', 0)->count();
    }
}
