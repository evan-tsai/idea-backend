<?php

namespace App\Composers;

class SuperAdminSidebarComposer
{
    public function compose($view)
    {
        $customerGroup = customers()->where('domain', '!=', config('admin.domain'))->sortBy('domain')->groupBy(function ($item) {
            return substr($item['domain'], 0, 1);
        })->map(function ($customers) {
            $active = $customers->reduce(function ($carry, $customer) {
                return $carry || active('customer/'. $customer->id . '/*');
            }, false);

            return [
                'active' => $active,
                'items' => $customers,
            ];
        });

        $view->with(compact('customerGroup'));
    }
}
