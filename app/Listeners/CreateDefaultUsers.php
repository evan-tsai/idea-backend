<?php

namespace App\Listeners;

use App\Events\CustomerCreated;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class CreateDefaultUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerCreated  $event
     * @return void
     */
    public function handle(CustomerCreated $event)
    {
        User::create([
            'name' => config('admin.username'),
            'customer_id' => $event->customer->id,
            'email' => config('admin.email'),
            'password' => bcrypt(config('admin.password')),
            'api_token' => bin2hex(str_random(30)),
        ]);

        // Create AWS Folder
        $folders = ['images', 'attachments', 'uploads'];
        $this->createFolders($event->customer->domain, $folders);
    }

    protected function createFolders($prefix, $folders)
    {
        $disk = Storage::disk('s3');

        foreach ($folders as $folder) {
            $folderName = $prefix . '/' . $folder;
            if (!$disk->has($folderName)) {
                $disk->makeDirectory($folderName);
            }
        }
    }
}
