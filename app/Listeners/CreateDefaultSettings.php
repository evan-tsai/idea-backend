<?php

namespace App\Listeners;

use App\Events\CustomerCreated;
use App\Models\Setting;

class CreateDefaultSettings
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerCreated  $event
     * @return void
     */
    public function handle(CustomerCreated $event)
    {
        if (!isAdminDomain($event->customer->domain)) {
            $configs = array_merge(
                config('default.product_settings'),
                config('default.contact_settings')
            );

            $settings = array_dot($configs);

            foreach ($settings as $key => $setting) {
                Setting::set($key, $setting, $event->customer->id);
            }
        }
    }
}
