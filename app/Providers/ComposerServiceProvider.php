<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeSidebar();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function composeSidebar()
    {
        view()->composer('layouts.admin.sidebar.superadmin.customer_list', '\App\Composers\SuperAdminSidebarComposer');
        view()->composer('layouts.admin._sidebar', '\App\Composers\SidebarComposer');
    }
}
