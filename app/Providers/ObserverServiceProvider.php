<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Models\Customer::observe(\App\Observers\CustomerObserver::class);
        \App\Models\ProductCategory::observe(\App\Observers\ProductCategoryObserver::class);
        \App\Models\Product::observe(\App\Observers\ProductObserver::class);
        \App\Models\User::observe(\App\Observers\UserObserver::class);
        \App\Models\ProductTagCategory::observe(\App\Observers\ProductTagCategoryObserver::class);
        \App\Models\ProductTag::observe(\App\Observers\ProductTagObserver::class);
        \App\Models\Setting::observe(\App\Observers\SettingObserver::class);
        \App\Models\ProductTranslation::observe(\App\Observers\ProductTranslationObserver::class);
        \App\Models\ArticleCategory::observe(\App\Observers\ArticleCategoryObserver::class);
        \App\Models\Article::observe(\App\Observers\ArticleObserver::class);
        \App\Models\ArticleTranslation::observe(\App\Observers\ArticleTranslationObserver::class);
        \App\Models\ArticleTag::observe(\App\Observers\ArticleTagObserver::class);
        \App\Models\Page::observe(\App\Observers\PageObserver::class);
        \App\Models\PageTranslation::observe(\App\Observers\PageTranslationObserver::class);
        \App\Models\ContentType::observe(\App\Observers\ContentTypeObserver::class);
        \App\Models\Content::observe(\App\Observers\ContentObserver::class);
        \App\Models\ContentTranslation::observe(\App\Observers\ContentTranslationObserver::class);
        \App\Models\MediaType::observe(\App\Observers\MediaTypeObserver::class);
        \App\Models\MediaCategory::observe(\App\Observers\MediaCategoryObserver::class);
        \App\Models\Media::observe(\App\Observers\MediaObserver::class);
        \App\Models\ContactInfo::observe(\App\Observers\ContactObserver::class);
        \App\Models\Social::observe(\App\Observers\SocialObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
