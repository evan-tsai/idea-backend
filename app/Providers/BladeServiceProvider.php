<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('formValidClass', function ($expression) {
            return sprintf('<?php echo $errors->has(%s) ? "is-invalid" : ""; ?>', $expression);
        });

        Blade::directive('requiredInput', function () {
           return '<span class="badge badge-danger">必填</span>';
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}