<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        $subdomain = $this->getSubdomain();

        Route::bind('articleCategory', function ($value) use ($subdomain) {
            return \App\Models\ArticleCategory::where('id', $value)->orWhere('type', $value)->whereHas('customer', function ($q) use ($subdomain) {
                $q->where('domain', $subdomain);
            })->first() ?? abort(404);
        });

        Route::bind('mediaType', function ($value) use ($subdomain) {
            return \App\Models\MediaType::where('id', $value)->orWhere('type', $value)->whereHas('customer', function ($q) use ($subdomain) {
                $q->where('domain', $subdomain);
            })->first() ?? abort(404);
        });

        Route::bind('contentType', function ($value) use ($subdomain) {
            return \App\Models\ContentType::where('id', $value)->orWhere('type', $value)->whereHas('customer', function ($q) use ($subdomain) {
                $q->where('domain', $subdomain);
            })->first() ?? abort(404);
        });

        Route::bind('page', function ($value) {
            return \App\Models\Page::where('id', $value)->orWhere('type', $value)->first() ?? abort(404);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->domain('{subdomain}.' . app_domain())
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace . '\Api')
            ->domain('{subdomain}.' . app_domain())
            ->group(base_path('routes/api.php'));
    }

    protected function getSubdomain()
    {
        if (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
            return explode('.', $_SERVER['HTTP_HOST'])[0];
        }

        return null;
    }
}
