<?php

namespace App\Writers;

use App\Models\ProductTag;
use App\Models\ProductTagTranslation;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;

class ProductTagWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return ProductTag::class;
    }

    protected function getTranslationClass()
    {
        return ProductTagTranslation::class;
    }

    public function rules(string $mode)
    {
        return [
            'category_id'   => 'required|numeric',
            'names.*'       => 'nullable|string',
            'description.*' => 'nullable|string',
            'image_path.*'  => 'nullable|string',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'category_id'   => '類別',
            'names.*'       => '名稱',
            'description.*' => '描述',
            'image_path.*'  => '代表圖片',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyTag($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyTag($validated);

        return $this->model;
    }

    protected function modifyTag(array $validated)
    {
        $this->model->customer_id = $this->customer()->id;

        array_has($validated, 'category_id') && ($this->model->category_id = $validated['category_id']);

        $this->model->save();

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
            'description' => $validated['description'] ?? null,
            'image_path' => $validated['image_path'] ?? null,
        ]);

        return $this->model;
    }
}
