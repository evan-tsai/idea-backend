<?php

namespace App\Writers;

use App\Writers\Base\BaseWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserModelWriter extends BaseWriter
{
    use BelongsToCustomer;

    public function __construct()
    {
        parent::__construct();

        $this->customer();
    }

    protected function getModelClass()
    {
        return User::class;
    }

    public function rules(string $mode)
    {
        $password = $mode === 'store'
            ? 'required|confirmed'
            : 'sometimes|confirmed';

        return [
            'name'       => 'required|unique:users,name,' . $this->model->id . ',id,customer_id,' . $this->customer->id,
            'email'       => 'required|unique:users,email,' . $this->model->id . ',id,customer_id,' . $this->customer->id,
            'password'   => $password,
            'status'    => 'nullable|boolean',
        ];
    }

    public function attributes()
    {
        return [
            'name'       => '名稱',
            'email'      => 'Email',
            'password'   => '密碼',
            'status'     => '啟用',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyUser($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyUser($validated);

        return $this->model;
    }

    protected function modifyUser(array $validated)
    {
        $this->model->customer_id = $this->customer->id;

        array_has($validated, 'name') && ($this->model->name = $validated['name']);
        array_has($validated, 'email') && ($this->model->email = $validated['email']);
        array_has($validated, 'password') && !empty($validated['password']) && ($this->model->password = Hash::make($validated['password']));
        array_has($validated, 'status') && ($this->model->locked = $validated['locked']);

        $this->model->save();

        return $this->model;
    }
}
