<?php

namespace App\Writers\Traits;

use App\Models\Customer;

trait BelongsToCustomer
{
    protected $customer;

    /**
     * Customer setter/getter
     *
     * @param \App\Models\Customer $customer
     * @return mixed   setter: $this, getter: Customer instance
     */
    public function customer(Customer $customer = null)
    {
        if (!$this->customer) {
            $this->customer = customer();
        }

        if (!$customer) {
            return $this->customer;
        }

        $this->customer = $customer;
        return $this;
    }
}
