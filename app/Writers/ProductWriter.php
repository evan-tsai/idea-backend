<?php

namespace App\Writers;

use App\Models\Product;
use App\Models\ProductTranslation;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return Product::class;
    }

    protected function getTranslationClass()
    {
        return ProductTranslation::class;
    }

    public function rules(string $mode)
    {
        return [
            'category_id'       => 'required|numeric',
            'names.*'           => 'nullable|string',
            'slug' => [
                'required',
                Rule::unique('products')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'model_name'        => 'nullable|string',
            'manufacture_date'  => 'nullable|date',
            'stock'             => 'nullable|integer|min:0',
            'description.*'     => 'nullable|string',
            'url.*'             => 'nullable|string',
            'image_path.*'      => 'nullable|string',
            'icon_path.*'       => 'nullable|string',
            'seo_title.*'       => 'nullable|string',
            'seo_description.*' => 'nullable|string',
            'seo_keyword.*'     => 'nullable|string',
            'sort'              => 'nullable|numeric',
            'tagIds'            => 'nullable|array',
            'editors'           => 'nullable|array',
            'galleries'         => 'nullable|array',
            'deleted_galleries' => 'nullable|array',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'category_id'       => '類別',
            'model_name'        => '型號',
            'manufacture_date'  => '製造日',
            'stock'             => '庫存',
            'names.*'           => '名稱',
            'slug'              => '自訂URL',
            'description.*'     => '描述',
            'url.*'             => '連結',
            'image_path.*'      => '代表圖片',
            'icon_path.*'       => '圖示',
            'galleries.*'       => '輪播圖',
            'editors.*'         => '編輯器',
            'seo_title.*'       => 'Seo Title',
            'seo_description.*' => 'Seo Description',
            'seo_keyword.*'     => 'Seo Keywords',
            'sort'              => '排序',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyProduct($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyProduct($validated);

        return $this->model;
    }

    protected function modifyProduct(array $validated)
    {
        $this->model->customer_id = $this->customer()->id;

        array_has($validated, 'category_id') && ($this->model->category_id = $validated['category_id']);
        array_has($validated, 'slug') && ($this->model->slug = $validated['slug']);
        array_has($validated, 'model_name') && ($this->model->model_name = $validated['model_name']);
        array_has($validated, 'manufacture_date') && ($this->model->manufacture_date = $validated['manufacture_date']);
        array_has($validated, 'stock') && ($this->model->stock = $validated['stock'] ?? 0);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
            'description' => $validated['description'] ?? null,
            'url' => $validated['url'] ?? null,
            'image_path' => $validated['image_path'] ?? null,
            'icon_path' => $validated['icon_path'] ?? null,
            'seo_title' => $validated['seo_title'] ?? null,
            'seo_description' => $validated['seo_description'] ?? null,
            'seo_keyword' => $validated['seo_keyword'] ?? null,
        ]);

        if (array_has($validated, 'tagIds')) {
            $tagIds = $this->modifyProductTags($validated['tagIds']);
            $this->model->tags()->sync($tagIds);
        }

        if (array_has($validated, 'editors')) {
            $this->saveTranslatedEditors($validated['editors']);
        }

        if (array_has($validated, 'galleries')) {
            $this->saveTranslatedCollections($validated['galleries']);
            $this->detachCollections($validated['deleted_galleries']);
        }

//        if (array_has($validated, 'banners')) {
//            $this->saveTranslatedCollections($validated['banners']);
//            $this->detachCollections($validated['deleted_banners']);
//        }

        return $this->model;
    }

    protected function modifyProductTags(array $tagTypeIds)
    {
        $ids = [];

        foreach ($tagTypeIds as $tagTypeId => $tagIds) {
            if (!is_array($tagIds)) {
                return $tagIds;
            }

            if (!count($tagIds)) {
                continue;
            }

            foreach ($tagIds as $key => $tagId) {
                $ids[$tagId] = ['sort' => $key];
            }
        }

        return $ids;
    }
}
