<?php

namespace App\Writers;

use App\Models\ProductCategory;
use App\Models\ProductCategoryTranslation;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductCategoryWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return ProductCategory::class;
    }

    protected function getTranslationClass()
    {
        return ProductCategoryTranslation::class;
    }

    public function rules(string $mode)
    {
        $parentId = 'required|numeric';
        if ($mode == 'update') {
            $parentId .= '|not_in:' . $this->model->id;
        }

        return [
            'parent_id'         => $parentId,
            'names.*'           => 'nullable|string',
            'slug' => [
                'nullable',
                Rule::unique('product_categories')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'title.*'           => 'nullable|string',
            'description.*'     => 'nullable|string',
            'image_path.*'      => 'nullable|string',
            'icon_path.*'       => 'nullable|string',
            'seo_title.*'       => 'nullable|string',
            'seo_description.*' => 'nullable|string',
            'seo_keyword.*'     => 'nullable|string',
            'sort'              => 'nullable|numeric',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'parent_id'         => '上層類別',
            'lang'              => '語言',
            'names.*'           => '名稱',
            'slug'              => '自訂URL',
            'title.*'           => '標題',
            'description.*'     => '描述',
            'image_path.*'      => '代表圖片',
            'icon_path.*'       => '圖示',
            'seo_title.*'       => 'Seo Title',
            'seo_description.*' => 'Seo Description',
            'seo_keyword.*'     => 'Seo Keywords',
            'sort'              => '排序',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    protected function modifyCategory(array $validated)
    {
        $this->model->customer_id = $this->customer()->id;

        array_has($validated, 'parent_id') && ($this->model->parent_id = $validated['parent_id']);
        array_has($validated, 'slug') && ($this->model->slug = $validated['slug']);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

//        if (!$this->model->slug) {
//            $this->model->slug = $this->model->id;
//            $this->model->save();
//        }

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
            'title' => $validated['title'] ?? null,
            'description' => $validated['description'] ?? null,
            'image_path' => $validated['image_path'] ?? null,
            'icon_path' => $validated['icon_path'] ?? null,
            'seo_title' => $validated['seo_title'] ?? null,
            'seo_description' => $validated['seo_description'] ?? null,
            'seo_keyword' => $validated['seo_keyword'] ?? null,
        ]);

        return $this->model;
    }
}
