<?php

namespace App\Writers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleTranslation;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ArticleWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return Article::class;
    }

    protected function getTranslationClass()
    {
        return ArticleTranslation::class;
    }

    public function rules(string $mode)
    {
        return [
            'article_type'      => 'required|string',
            'names.*'           => 'nullable|string',
            'slug' => [
                'required',
                Rule::unique('articles')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'publish_start'     => 'nullable|date',
            'publish_end'       => 'nullable|date',
            'image_path.*'      => 'nullable|string',
            'author.*'          => 'nullable|string',
            'seo_title.*'       => 'nullable|string',
            'seo_description.*' => 'nullable|string',
            'seo_keyword.*'     => 'nullable|string',
            'sort'              => 'nullable|numeric',
            'editors'           => 'nullable|array',
            'tagIds'            => 'nullable|array',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'publish_start'     => '上架日',
            'publish_end'       => '下架日',
            'names.*'           => '名稱',
            'slug'              => '自訂URL',
            'image_path.*'      => '代表圖片',
            'author.*'          => '作者',
            'editors.*'         => '編輯器',
            'seo_title.*'       => 'Seo Title',
            'seo_description.*' => 'Seo Description',
            'seo_keyword.*'     => 'Seo Keywords',
            'sort'              => '排序',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyArticle($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyArticle($validated);

        return $this->model;
    }

    protected function modifyArticle(array $validated)
    {
        $articleType = ArticleCategory::where('customer_id', customer('id'))
            ->where('type', $validated['article_type'])
            ->first()
            ->id;

        $this->model->customer_id = $this->customer()->id;
        $this->model->category_id = $articleType;

        array_has($validated, 'slug') && ($this->model->slug = $validated['slug']);
        array_has($validated, 'publish_start') && ($this->model->publish_start = $validated['publish_start']);
        array_has($validated, 'publish_end') && ($this->model->publish_end = $validated['publish_end']);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
            'image_path' => $validated['image_path'] ?? null,
            'author' => $validated['author'] ?? null,
            'seo_title' => $validated['seo_title'] ?? null,
            'seo_description' => $validated['seo_description'] ?? null,
            'seo_keyword' => $validated['seo_keyword'] ?? null,
        ]);

        if (array_has($validated, 'tagIds')) {
            $tagIds = $this->modifyArticleTags($validated['tagIds']);
            $this->model->tags()->sync($tagIds);
        }

        if (array_has($validated, 'editors')) {
            $this->saveTranslatedEditors($validated['editors']);
        }

        return $this->model;
    }

    protected function modifyArticleTags(array $tagIds)
    {
        $ids = [];

        foreach ($tagIds as $key => $tagId) {
            $ids[$tagId] = ['sort' => $key];
        }

        return $ids;
    }
}
