<?php

namespace App\Writers;

use App\Models\Media;
use App\Models\MediaTranslation;
use App\Models\MediaType;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;

class MediaWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return Media::class;
    }

    protected function getTranslationClass()
    {
        return MediaTranslation::class;
    }

    public function rules(string $mode)
    {
        return [
            'media_type'    => 'required|string',
            'category_id'   => 'required|numeric',
            'names.*'       => 'nullable|string',
            'image_path.*'  => 'nullable|string',
            'description.*' => 'nullable|string',
            'url.*'         => 'nullable|string',
            'sort'          => 'nullable|numeric',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'category_id'   => '類別',
            'names.*'       => '名稱',
            'url'           => '連結',
            'description.*' => '描述',
            'image_path.*'  => '代表圖片',
            'sort'          => '排序',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyMedia($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyMedia($validated);

        return $this->model;
    }

    protected function modifyMedia(array $validated)
    {
        $mediaType = MediaType::where('customer_id', customer('id'))
            ->where('type', $validated['media_type'])
            ->first()
            ->id;

        $this->model->customer_id = $this->customer()->id;
        $this->model->type_id = $mediaType;

        array_has($validated, 'category_id') && ($this->model->category_id = $validated['category_id']);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
            'description' => $validated['description'] ?? null,
            'image_path' => $validated['image_path'] ?? null,
            'url' => $validated['url'] ?? null,
        ]);

        return $this->model;
    }
}
