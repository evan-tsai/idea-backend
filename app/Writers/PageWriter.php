<?php

namespace App\Writers;

use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Setting;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PageWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    public function __construct($customer = null)
    {
        parent::__construct();

        if ($customer) {
            $this->customer($customer);
        }
    }

    protected function getModelClass()
    {
        return Page::class;
    }

    protected function getTranslationClass()
    {
        return PageTranslation::class;
    }

    public function rules(string $mode)
    {
        if ($mode === 'save') {
            return [
                'seo_title.*'       => 'nullable|string',
                'seo_description.*' => 'nullable|string',
                'seo_keyword.*'     => 'nullable|string',
                'galleries'         => 'nullable|array',
                'deleted_galleries' => 'nullable|array',
                'banners'           => 'nullable|array',
                'deleted_banners'   => 'nullable|array',
                'editors'           => 'nullable|array',
            ];
        }
        return [
            'name' => 'required|string',
            'type' => [
                'required',
                Rule::unique('pages')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'sort' => 'nullable|numeric',
            'page' => 'nullable|array',
        ];
    }

    public function attributes()
    {
        return [
            'name'              => '名稱',
            'sort'              => '排序',
            'banners.*'         => '橫幅',
            'galleries.*'       => '輪播圖',
            'editors.*'         => '編輯器',
            'seo_title.*'       => 'Seo Title',
            'seo_description.*' => 'Seo Description',
            'seo_keyword.*'     => 'Seo Keywords',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyPage($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyPage($validated);

        return $this->model;
    }

    public function save(Request $request)
    {
        $validated = $request->validate($this->rules('save'), [], $this->attributes());

        $this->modifyPageTranslations($validated);

        return $this->model;
    }

    protected function modifyPage(array $validated)
    {
        $this->model->customer_id = $this->customer->id;

        array_has($validated, 'name') && ($this->model->name = $validated['name']);
        array_has($validated, 'type') && ($this->model->type = $validated['type']);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        if (array_has($validated, 'page')) {
            $this->setSettings($validated, 'page');
        }

        return $this->model;
    }

    protected function modifyPageTranslations(array $validated)
    {
        $this->saveTranslatedFields([
            'seo_title' => $validated['seo_title'] ?? null,
            'seo_description' => $validated['seo_description'] ?? null,
            'seo_keyword' => $validated['seo_keyword'] ?? null,
        ]);

        if (array_has($validated, 'banners')) {
            $this->saveTranslatedCollections($validated['banners']);
            $this->detachCollections($validated['deleted_banners']);
        }

        if (array_has($validated, 'galleries')) {
            $this->saveTranslatedCollections($validated['galleries']);
            $this->detachCollections($validated['deleted_galleries']);
        }

        if (array_has($validated, 'editors')) {
            $this->saveTranslatedEditors($validated['editors']);
        }
    }

    protected function setSettings($validated, $type)
    {
        $settings = array_dot($validated[$type]);
        foreach ($settings as $key => $item) {
            Setting::set($type . '.' . $validated['type'] . '.' . $key, $item, $this->customer->id);
        }
    }
}
