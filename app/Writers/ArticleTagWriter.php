<?php

namespace App\Writers;

use App\Models\ArticleCategory;
use App\Models\ArticleTag;
use App\Models\ArticleTagTranslation;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;

class ArticleTagWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return ArticleTag::class;
    }

    protected function getTranslationClass()
    {
        return ArticleTagTranslation::class;
    }

    public function rules(string $mode)
    {
        return [
            'article_type' => 'required|string',
            'names.*'      => 'nullable|string',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'names.*' => '名稱',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyTag($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyTag($validated);

        return $this->model;
    }

    protected function modifyTag(array $validated)
    {
        $articleType = ArticleCategory::where('type', $validated['article_type'])->first()->id;

        $this->model->customer_id = $this->customer()->id;
        $this->model->category_id = $articleType;

        $this->model->save();

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
        ]);

        return $this->model;
    }
}
