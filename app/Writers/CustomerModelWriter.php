<?php

namespace App\Writers;

use App\Writers\Base\BaseWriter;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerModelWriter extends BaseWriter
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getModelClass()
    {
        return Customer::class;
    }

    public function rules(string $mode)
    {
        return [
            'domain'       => 'required|unique:customers,domain,' . $this->model->id,
            'logo'         => 'nullable|string',
            'name'         => 'required|string',
            'status'       => 'nullable|boolean',
            'default_lang' => 'required|exists:languages,id|in_array:languages.*',
            'languages'    => 'required|array',
        ];
    }

    public function attributes()
    {
        return [
            'domain'       => '網域',
            'logo'         => 'Logo',
            'name'         => '名稱',
            'status'       => '狀態',
            'default_lang' => '預設語系',
            'languages'    => '啟用語言',
            'languages.*'  => '啟用語言',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $customer = $this->modifyCustomer($validated);

        event(new \App\Events\CustomerCreated($customer));

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyCustomer($validated);

        return $this->model;
    }

    protected function modifyCustomer(array $validated)
    {
        array_has($validated, 'domain') && ($this->model->domain = $validated['domain']);
        array_has($validated, 'logo') && ($this->model->logo = $validated['logo']);
        array_has($validated, 'name') && ($this->model->name = $validated['name']);
        array_has($validated, 'status') && ($this->model->status = $validated['status']);
        array_has($validated, 'default_lang') && ($this->model->default_lang = $validated['default_lang']);

        $this->model->save();

        $languages = $this->sortLanguages($validated['languages']);
        $this->model->languages()->sync($languages);

        return $this->model;
    }

    protected function sortLanguages($languages)
    {
        $result = [];
        $count = 1;
        foreach ($languages as $value) {
            // Put default lang first
            $sort = $value === $this->model->default_lang ? 0 : $count;
            $result[$value] = ['sort' => $sort];
            $count++;
        }

        return collect($result);
    }
}
