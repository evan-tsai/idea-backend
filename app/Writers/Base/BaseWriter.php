<?php


namespace App\Writers\Base;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class BaseWriter
{
    protected $model;

    public function __construct()
    {
        $className = $this->getModelClass();

        $this->model = new $className;
    }

    public function for(Model $model)
    {
        if ($model && !is_a($model, $this->getModelClass())) {
            throw new \UnexpectedValueException('Model is not a ' . $this->getModelClass());
        }

        $this->model = $model;

        return $this;
    }

    abstract protected function getModelClass();

    abstract public function rules(string $mode);

    abstract public function attributes();

    abstract public function store(Request $request);

    abstract public function update(Request $request);
}