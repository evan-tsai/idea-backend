<?php


namespace App\Writers\Base;


use App\Models\Collection;
use App\Models\Editor;
use App\Writers\Traits\BelongsToCustomer;

abstract class BaseTranslationWriter extends BaseWriter
{
    use BelongsToCustomer;

    public function __construct()
    {
        parent::__construct();

        $this->customer(customer());
    }

    abstract protected function getTranslationClass();

    protected function saveTranslatedFields($fields)
    {
        if (!method_exists($this->getModelClass(), 'translations')) {
            throw new \UnexpectedValueException('Model does not have translation relationship');
        }

        $newTranslations = [];
        $existingTranslations = $this->model->translations;

        $className = $this->getTranslationClass();

        foreach ($this->customer->languages as $language) {
            $translation = $existingTranslations->where('lang', $language->id)->first() ?: new $className;
            $translation->lang = $language->id;
            foreach ($fields as $key => $field) {
                if (!array_has($field, $language->id)) continue;
                $translation->{$key} = $field[$language->id];
            }
            $translation->exists ? $translation->save() : ($newTranslations[] = $translation);
        }

        $this->model->translations()->saveMany($newTranslations);
    }

    protected function saveTranslatedEditors(array $multiLanguageEditors)
    {
        $this->model->load('translations');

        foreach ($multiLanguageEditors as $language => $editors) {
            $model = $this->model->translations->where('lang', $language)->first();

            $newEditors = [];
            $existingEditors = $model->editors;

            foreach ($editors as $key => $content) {
                $editor = $existingEditors->where('sort', $key)->first() ?: new Editor;

                $editor->fill([
                    'content' => $content,
                    'sort'   => $key,
                ]);

                $editor->exists
                    ? $editor->save()
                    : ($newEditors[] = $editor);
            }

            $model->editors()->saveMany($newEditors);
        }
    }

    public function saveTranslatedCollections(array $multiLanguageCollections)
    {
        $this->model->load('translations');

        foreach ($multiLanguageCollections as $language => $collections) {
            $model = $this->model->translations->where('lang', $language)->first();

            $newCollections = [];
            $existingCollections = $model->collections;
            $decodedCollections = json_decode($collections, true);

            foreach ($decodedCollections as $key => $item) {
                $collection = $existingCollections->where('id', array_get($item, 'id'))->first() ?: new Collection;

                if (empty($item['image_path'])) {
                    continue;
                }

                $collection->fill([
                    'image_path'  => $item['image_path'] ?: '',
                    'type'        => $item['type'] ?: '',
                    'name'        => $item['name'] ?: '',
                    'description' => $item['description'] ?: '',
                    'url'         => $item['url'] ?: '',
                    'video_url'   => $item['video_url'] ?: '',
                    'sort'        => $item['sort'] ?: config('form.default.sort'),
                    'status'      => isset($item['status']) ? $item['status'] : null,
                ]);

                $collection->exists
                    ? $collection->save()
                    : ($newCollections[] = $collection);
            }

            $model->collections()->saveMany($newCollections);
        }
    }

    public function detachCollections($multiLanguageIds)
    {
        foreach ($multiLanguageIds as $language => $deletedIds) {
            $model = $this->model->translations->where('lang', $language)->first();
            $decodedIds = json_decode($deletedIds, true);

            if (count($decodedIds)) {
                $model->collections()->detach($decodedIds);
                Collection::whereIn('id', $decodedIds)->delete();
            }
        }
    }
}