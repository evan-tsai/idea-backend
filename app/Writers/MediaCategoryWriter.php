<?php

namespace App\Writers;

use App\Models\ArticleCategory;
use App\Models\ArticleTag;
use App\Models\ArticleTagTranslation;
use App\Models\MediaCategory;
use App\Models\MediaCategoryTranslation;
use App\Models\MediaType;
use App\Writers\Base\BaseTranslationWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;

class MediaCategoryWriter extends BaseTranslationWriter
{
    use BelongsToCustomer;

    protected function getModelClass()
    {
        return MediaCategory::class;
    }

    protected function getTranslationClass()
    {
        return MediaCategoryTranslation::class;
    }

    public function rules(string $mode)
    {
        return [
            'media_type' => 'required|string',
            'names.*'    => 'nullable|string',
            'sort'       => 'nullable|numeric',

            'names.'.$this->customer->default_lang => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'names.*' => '名稱',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    protected function modifyCategory(array $validated)
    {
        $mediaType = MediaType::where('type', $validated['media_type'])->first()->id;

        $this->model->customer_id = $this->customer()->id;
        $this->model->type_id = $mediaType;

        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        $this->saveTranslatedFields([
            'name' => $validated['names'] ?? null,
        ]);

        return $this->model;
    }
}
