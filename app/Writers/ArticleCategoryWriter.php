<?php

namespace App\Writers;

use App\Models\ArticleCategory;
use App\Models\Setting;
use App\Writers\Base\BaseWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ArticleCategoryWriter extends BaseWriter
{
    use BelongsToCustomer;

    public function __construct($customer)
    {
        parent::__construct();

        $this->customer($customer);
    }

    protected function getModelClass()
    {
        return ArticleCategory::class;
    }

    public function rules(string $mode)
    {
        return [
            'name' => [
                'required',
                Rule::unique('article_categories')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'type' => [
                'required',
                Rule::unique('article_categories')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'icon' => 'nullable|string',
            'sort' => 'nullable|numeric',
            'article' => 'nullable|array',
        ];
    }

    public function attributes()
    {
        return [
            'name'     => '名稱',
            'type'     => '類型',
            'icon'     => '圖示',
            'sort'     => '排序',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    protected function modifyCategory(array $validated)
    {
        $this->model->customer_id = $this->customer->id;

        array_has($validated, 'name') && ($this->model->name = $validated['name']);
        array_has($validated, 'type') && ($this->model->type = $validated['type']);
        array_has($validated, 'icon') && ($this->model->icon = $validated['icon']);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        if (array_has($validated, 'article')) {
            $this->setSettings($validated, 'article');
        }

        return $this->model;
    }

    protected function setSettings($validated, $type)
    {
        $settings = array_dot($validated[$type]);
        foreach ($settings as $key => $item) {
            Setting::set($type . '.' . $validated['type'] . '.' . $key, $item, $this->customer->id);
        }
    }
}
