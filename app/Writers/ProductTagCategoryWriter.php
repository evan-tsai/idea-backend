<?php

namespace App\Writers;

use App\Models\ProductTagCategory;
use App\Writers\Base\BaseWriter;
use App\Writers\Traits\BelongsToCustomer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductTagCategoryWriter extends BaseWriter
{
    use BelongsToCustomer;

    public function __construct($customer)
    {
        parent::__construct();

        $this->customer($customer);
    }

    protected function getModelClass()
    {
        return ProductTagCategory::class;
    }

    public function rules(string $mode)
    {
        return [
            'name'     => [
                'required',
                Rule::unique('product_tag_categories')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'type'     => [
                'required',
                Rule::unique('product_tag_categories')->where(function ($query) {
                    return $query->where('customer_id', $this->customer->id);
                })->ignore($this->model->id)
            ],
            'multiple' => 'required|numeric',
            'sort'     => 'nullable|integer|min:0',
        ];
    }

    public function attributes()
    {
        return [
            'name'     => '名稱',
            'type'     => '類型',
            'sort'     => '排序',
            'multiple' => '多選',
        ];
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->rules('store'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    public function update(Request $request)
    {
        $validated = $request->validate($this->rules('update'), [], $this->attributes());

        $this->modifyCategory($validated);

        return $this->model;
    }

    protected function modifyCategory(array $validated)
    {
        $this->model->customer_id = $this->customer->id;

        array_has($validated, 'name') && ($this->model->name = $validated['name']);
        array_has($validated, 'type') && ($this->model->type = $validated['type']);
        array_has($validated, 'multiple') && ($this->model->multiple = $validated['multiple']);
        array_has($validated, 'sort') && ($this->model->sort = $validated['sort'] ?? config('form.default.sort'));

        $this->model->save();

        return $this->model;
    }
}
