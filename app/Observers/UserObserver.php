<?php


namespace App\Observers;


use App\Models\User;

class UserObserver
{
    public function creating(User $user) {
        $user->api_token = bin2hex(str_random(30));
    }
}