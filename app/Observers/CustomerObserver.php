<?php

namespace App\Observers;

use App\Models\Customer;

class CustomerObserver
{
    public function deleting(Customer $customer)
    {
        $customer->languages()->detach();
        $customer->users()->delete();
    }
}
