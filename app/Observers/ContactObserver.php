<?php


namespace App\Observers;


use App\Observers\Base\CacheObserver;

class ContactObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'contact';
    }
}