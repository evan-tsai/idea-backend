<?php


namespace App\Observers;


use App\Models\ProductCategory;
use App\Observers\Base\CacheObserver;

class ProductCategoryObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return ['product-category', 'products'];
    }

    /**
     * @param ProductCategory $productCategory
     * @throws \Exception
     */
    public function deleting(ProductCategory $productCategory)
    {
        $childCount = $productCategory->children->count();
        if ($childCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$childCount.'個子類別');
        }

        $productCount = $productCategory->products->count();
        if ($productCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$productCount.'個產品');
        }

        $productCategory->translations()->delete();
    }
}