<?php


namespace App\Observers;



use App\Models\Setting;

class SettingObserver
{
    protected function flushCache()
    {
        \Cache::forget('settings.all');
    }

    public function created(Setting $setting)
    {
        $this->flushCache();
    }

    public function updated(Setting $setting)
    {
        $this->flushCache();
    }

    public function deleted(Setting $setting)
    {
        $this->flushCache();
    }
}