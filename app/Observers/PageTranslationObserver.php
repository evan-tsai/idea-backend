<?php


namespace App\Observers;


use App\Models\Collection;
use App\Models\PageTranslation;
use App\Observers\Base\CacheObserver;

class PageTranslationObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'page';
    }

    public function deleting(PageTranslation $translation)
    {
        $translation->editors()->delete();
        $collectionIds = $translation->collections()->get()->pluck('id');
        $translation->collections()->detach();
        Collection::whereIn('id', $collectionIds)->delete();
    }
}