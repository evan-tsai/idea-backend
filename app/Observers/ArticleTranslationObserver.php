<?php


namespace App\Observers;


use App\Models\ArticleTranslation;
use App\Observers\Base\CacheObserver;

class ArticleTranslationObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'articles';
    }

    public function deleting(ArticleTranslation $translation)
    {
        $translation->editors()->delete();
    }
}