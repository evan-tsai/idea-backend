<?php


namespace App\Observers;


use App\Models\Page;
use App\Models\Setting;
use App\Observers\Base\CacheObserver;

class PageObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'page';
    }

    public function deleting(Page $page)
    {
        // Use foreach to trigger translation observer
        foreach ($page->translations as $translation) {
            $translation->delete();
        }

        Setting::where('customer_id', $page->customer_id)
            ->where('key', 'like', 'page.' . $page->type . '.%')
            ->delete();
    }
}