<?php


namespace App\Observers\Base;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

abstract class CacheObserver
{
    abstract protected function getCacheKey();

    public function created(Model $model)
    {
        $this->flushCache($model);
    }

    public function updated(Model $model)
    {
        $this->flushCache($model);
    }

    public function deleted(Model $model)
    {
        $this->flushCache($model);
    }

    protected function flushCache($model)
    {
        $customerId = ($model->customer->domain ?? null) ?: customer('domain');

        if (is_array($this->getCacheKey())) {
            foreach ($this->getCacheKey() as $value) {
                Cache::tags($customerId . '.' . $value)->flush();
            }
        } else {
            Cache::tags($customerId . '.' . $this->getCacheKey())->flush();
        }
    }
}