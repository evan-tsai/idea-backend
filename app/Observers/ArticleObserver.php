<?php


namespace App\Observers;


use App\Models\Article;
use App\Observers\Base\CacheObserver;

class ArticleObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'articles';
    }

    public function deleting(Article $article)
    {
        // Use foreach to trigger translation observer
        foreach ($article->translations as $translation) {
            $translation->delete();
        }
        $article->tags()->detach();
    }
}