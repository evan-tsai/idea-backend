<?php


namespace App\Observers;


use App\Observers\Base\CacheObserver;

class SocialObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'social';
    }
}