<?php


namespace App\Observers;


use App\Models\ProductTag;
use App\Observers\Base\CacheObserver;

class ProductTagObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return ['product-tags', 'products'];
    }

    public function deleting(ProductTag $productTag)
    {
        $productTag->translations()->delete();
    }
}