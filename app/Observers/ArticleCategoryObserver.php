<?php


namespace App\Observers;


use App\Models\ArticleCategory;
use App\Models\Setting;

class ArticleCategoryObserver
{
    public function deleting(ArticleCategory $category)
    {
        $articleCount = $category->articles->count();
        if ($articleCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$articleCount.'個文章');
        }

        $tagCount = $category->tags->count();
        if ($tagCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$tagCount.'個標籤');
        }

        Setting::where('customer_id', $category->customer_id)
            ->where('key', 'like', 'article.' . $category->type . '.%')
            ->delete();
    }
}