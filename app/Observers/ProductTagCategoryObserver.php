<?php


namespace App\Observers;


use App\Models\ProductTagCategory;

class ProductTagCategoryObserver
{
    public function deleting(ProductTagCategory $productTagCategory)
    {
        $tagCount = $productTagCategory->tags->count();
        if ($tagCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$tagCount.'個標籤');
        }
    }
}