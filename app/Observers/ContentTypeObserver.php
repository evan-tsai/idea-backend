<?php


namespace App\Observers;


use App\Models\ContentType;
use App\Models\Setting;

class ContentTypeObserver
{
    /**
     * @param ContentType $contentType
     * @throws \Exception
     */
    public function deleting(ContentType $contentType)
    {
        $contentCount = $contentType->contents->count();
        if ($contentCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$contentCount.'個內容');
        }

        Setting::where('customer_id', $contentType->customer_id)
            ->where('key', 'like', 'content.' . $contentType->type . '.%')
            ->delete();
    }
}