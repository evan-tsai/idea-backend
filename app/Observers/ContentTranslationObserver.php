<?php


namespace App\Observers;


use App\Models\ContentTranslation;
use App\Observers\Base\CacheObserver;

class ContentTranslationObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'content';
    }

    /**
     * @param ContentTranslation $translation
     */
    public function deleting(ContentTranslation $translation)
    {
        $translation->editors()->delete();
    }
}