<?php


namespace App\Observers;


use App\Models\MediaType;
use App\Models\Setting;

class MediaTypeObserver
{
    /**
     * @param MediaType $mediaType
     * @throws \Exception
     */
    public function deleting(MediaType $mediaType)
    {
        $categoriesCount = $mediaType->categories->count();
        if ($categoriesCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$categoriesCount.'個類別');
        }

        Setting::where('customer_id', $mediaType->customer_id)
            ->where('key', 'like', 'media.' . $mediaType->type . '.%')
            ->delete();
    }
}