<?php


namespace App\Observers;


use App\Models\Collection;
use App\Models\ProductTranslation;
use App\Observers\Base\CacheObserver;

class ProductTranslationObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return ['product-category', 'products'];
    }

    public function deleting(ProductTranslation $translation)
    {
        $translation->editors()->delete();
        $collectionIds = $translation->collections()->get()->pluck('id');
        $translation->collections()->detach();
        Collection::whereIn('id', $collectionIds)->delete();
    }
}