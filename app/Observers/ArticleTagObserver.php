<?php


namespace App\Observers;


use App\Models\ArticleTag;
use App\Observers\Base\CacheObserver;

class ArticleTagObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'article-tags';
    }

    public function deleting(ArticleTag $articleTag)
    {
        $articleTag->translations()->delete();
    }
}