<?php


namespace App\Observers;


use App\Models\MediaCategory;
use App\Observers\Base\CacheObserver;

class MediaCategoryObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'media-category';
    }

    /**
     * @param MediaCategory $mediaCategory
     * @throws \Exception
     */
    public function deleting(MediaCategory $mediaCategory)
    {
        $mediaCount = $mediaCategory->media->count();
        if ($mediaCount > 0) {
            throw new \Exception('無法刪除<br />此類別底下有'.$mediaCount.'個媒體');
        }

        $mediaCategory->translations()->delete();
    }
}