<?php


namespace App\Observers;


use App\Models\Product;
use App\Observers\Base\CacheObserver;

class ProductObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return ['product-category', 'products'];
    }

    public function deleting(Product $product)
    {
        // Use foreach to trigger translation observer
        foreach ($product->translations as $translation) {
            $translation->delete();
        }
        $product->tags()->detach();
    }
}