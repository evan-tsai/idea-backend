<?php


namespace App\Observers;


use App\Models\Content;
use App\Observers\Base\CacheObserver;

class ContentObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'content';
    }

    /**
     * @param Content $content
     * @throws \Exception
     */
    public function deleting(Content $content)
    {
        foreach ($content->translations as $translation) {
            $translation->delete();
        }
    }
}