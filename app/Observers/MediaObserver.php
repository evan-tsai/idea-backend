<?php


namespace App\Observers;


use App\Models\Media;
use App\Observers\Base\CacheObserver;

class MediaObserver extends CacheObserver
{
    protected function getCacheKey()
    {
        return 'media';
    }

    /**
     * @param Media $media
     */
    public function deleting(Media $media)
    {
        $media->translations()->delete();
    }
}