<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class VerifyApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('X-Auth-Token')) {
            return abort(401);
        }

        $user = User::where([
            'customer_id' => customer('id'),
            'api_token' => $request->header('X-Auth-Token'),
        ])->first();

        if (!$user) {
            return abort(403);
        }

        return $next($request);
    }
}
