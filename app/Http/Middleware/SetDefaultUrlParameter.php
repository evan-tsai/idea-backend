<?php

namespace App\Http\Middleware;

use Closure;

class SetDefaultUrlParameter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app('url')->defaults([
            'subdomain' => $request->realDomainCustomer->domain,
        ]);

        return $next($request);
    }
}
