<?php

namespace App\Http\Middleware;

use Closure;

class VerifySubDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $subDomain = $request->subdomain;

        if (!$subDomain) {
            return abort('404');
        }

        if ($customer = $this->getCustomer($subDomain)) {
            $request->realDomainCustomer = $customer;
            $request->route()->forgetParameter('subdomain');

            return $next($request);
        }

        return abort('404');
    }

    protected function getCustomer($subDomain)
    {
        $customer = customers()->where('domain', $subDomain);

        // Only check status for backend (api remains available)
        if (in_array('web', request()->route()->middleware())) {
            $customer = $customer->where('status', true);
        }

        return $customer->first();
    }
}
