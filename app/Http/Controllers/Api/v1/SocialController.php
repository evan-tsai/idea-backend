<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Queries\SocialQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SocialController extends Controller
{
    public function get(SocialQuery $query, Request $request)
    {
        $tag = customer('domain') . '.social';

        if (!$socials = Cache::tags($tag)->get(cache_url())) {
            $socials = $query->frontend()->get();

            Cache::tags($tag)->put(cache_url(), $socials, config('api.cache_minutes'));
        }

        if (!$socials) {
            return $this->failResponse('Socials not found.');
        }

        return $this->successResponse($socials);
    }
}