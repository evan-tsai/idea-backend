<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\ContactInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ContactController extends Controller
{
    public function get(Request $request)
    {
        $tag = customer('domain') . '.contact';

        if (!$contactInfo = Cache::tags($tag)->get(cache_url())) {
            $contactInfo = resolve(ContactInfo::class)->getTranslations(request()->input('lang', customer('default_lang')));

            Cache::tags($tag)->put(cache_url(), $contactInfo, config('api.cache_minutes'));
        }

        if (!$contactInfo) {
            return $this->failResponse('Contact Info not found.');
        }

        return $this->successResponse($contactInfo);
    }
}