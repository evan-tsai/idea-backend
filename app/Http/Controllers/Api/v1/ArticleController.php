<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\ArticleCategory;
use App\Queries\ArticleQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ArticleController extends Controller
{
    public function get(ArticleCategory $articleCategory, ArticleQuery $query, Request $request)
    {
        $tag = customer('domain') . '.articles';

        if (!$articles = Cache::tags($tag)->get(cache_url())) {
            $articles = $query->frontend()
                ->byType($articleCategory->type, 'category')
                ->get();

            Cache::tags($tag)->put(cache_url(), $articles, config('api.cache_minutes'));
        }

        if (!$articles) {
            return $this->failResponse('Articles not found.');
        }

        return $this->successResponse($articles);
    }
}