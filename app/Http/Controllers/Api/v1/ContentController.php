<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\ContentType;
use App\Queries\ContentQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ContentController extends Controller
{
    public function get(ContentType $contentType, ContentQuery $query, Request $request)
    {
        $tag = customer('domain') . '.content';

        if (!$content = Cache::tags($tag)->get(cache_url())) {
            $content = $query->frontend()->byType($contentType->type)->get();

            Cache::tags($tag)->put(cache_url(), $content, config('api.cache_minutes'));
        }

        if (!$content) {
            return $this->failResponse('Content not found.');
        }

        return $this->successResponse($content);
    }
}