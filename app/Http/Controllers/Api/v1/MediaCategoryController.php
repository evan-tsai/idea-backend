<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\MediaType;
use App\Queries\MediaCategoryQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MediaCategoryController extends Controller
{
    public function get(MediaType $mediaType, MediaCategoryQuery $query, Request $request)
    {
        $tag = customer('domain') . '.media-category';

        if (!$mediaCategory = Cache::tags($tag)->get(cache_url())) {
            $mediaCategory = $query->frontend()->byType($mediaType->type)->get();

            Cache::tags($tag)->put(cache_url(), $mediaCategory, config('api.cache_minutes'));
        }

        if (!$mediaCategory) {
            return $this->failResponse('Media Categories not found.');
        }

        return $this->successResponse($mediaCategory);
    }
}