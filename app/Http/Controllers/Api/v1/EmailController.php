<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Mail\ContactForm;
use App\Models\Email;
use App\Rules\ValidRecaptcha;
use App\Services\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'file' => 'file|max:1000',
//            'g-recaptcha-response' => ['required', new ValidRecaptcha]
        ]);

        if ($validator->fails()) {
            return $this->failResponse('寄信失敗', $validator->errors()->all());
        }

        $receivers = json_decode(setting('email.receivers.items'), true);

        if (empty($receivers)) {
            return $this->failResponse('未設定收件人');
        }

        $type = $request->input('type', '聯絡信');
        $title = customer('name') . ' ' . $type;
        $data = $request->except(['type', 'file', 'g-recaptcha-response']);
        $file = Upload::uploadToS3($request->file('file'), 'attachments');

        try {
            $mailable = new ContactForm($title, $data, $file);

            Mail::to($receivers)->send($mailable);

            $this->saveRecord($type, $title, $data, $file);

            return $this->successResponse('寄信成功');
        } catch (\Exception $e) {
            return $this->failResponse('寄信失敗');
        }
    }

    protected function saveRecord(string $type, string $title, array $fields, $file)
    {
        try {
            $record = new Email;
            $record->customer_id = customer('id');
            $record->type = $type;
            $record->title = $title;
            $record->fields = $fields;
            $record->attachment = $file['path'];
            $record->save();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}