<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\ProductTag;
use App\Queries\ProductTagQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductTagController extends Controller
{
    public function get(ProductTagQuery $query, Request $request)
    {
        $tag = customer('domain') . '.product-tags';

        if (!$productTags = Cache::tags($tag)->get(cache_url())) {
            $productTags = $query->frontend()->get();

            Cache::tags($tag)->put(cache_url(), $productTags, config('api.cache_minutes'));
        }

        if (!$productTags) {
            return $this->failResponse('Tags not found.');
        }

        return $this->successResponse($productTags);
    }
}