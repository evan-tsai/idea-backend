<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\MediaType;
use App\Queries\MediaQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MediaController extends Controller
{
    public function get(MediaType $mediaType, MediaQuery $query, Request $request)
    {
        $tag = customer('domain') . '.media';

        if (!$media = Cache::tags($tag)->get(cache_url())) {
            $media = $query->frontend($mediaType)->get();

            Cache::tags($tag)->put(cache_url(), $media, config('api.cache_minutes'));
        }

        if (!$media) {
            return $this->failResponse('Media not found.');
        }

        return $this->successResponse($media);
    }
}