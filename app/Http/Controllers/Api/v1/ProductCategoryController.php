<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\ProductCategory;
use App\Queries\ProductCategoryQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductCategoryController extends Controller
{
    public function get(ProductCategoryQuery $query, Request $request)
    {
        $tag = customer('domain') . '.product-category';

        if (!$categories = Cache::tags($tag)->get(cache_url())) {
            $categories = ($request->filled('id') || $request->filled('slug')) ? $query->frontend()->get() : $query->frontend()->tree();

            Cache::tags($tag)->put(cache_url(), $categories, config('api.cache_minutes'));
        }

        if (!$categories) {
            return $this->failResponse('Categories not found.');
        }

        return $this->successResponse($categories);
    }
}