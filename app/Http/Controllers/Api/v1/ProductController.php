<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\Product;
use App\Queries\ProductQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
    public function get(ProductQuery $query, Request $request)
    {
        $tag = customer('domain') . '.products';

        if (!$products = Cache::tags($tag)->get(cache_url())) {
            $products = $query->frontend()->get();

            Cache::tags($tag)->put(cache_url(), $products, config('api.cache_minutes'));
        }

        if (!$products) {
            return $this->failResponse('Products not found.');
        }

        return $this->successResponse($products);
    }
}