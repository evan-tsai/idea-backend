<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Models\ArticleCategory;
use App\Queries\ArticleTagQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ArticleTagController extends Controller
{
    public function get(ArticleCategory $articleCategory, ArticleTagQuery $query, Request $request)
    {
        $tag = customer('domain') . '.article-tags';

        if (!$articleTags = Cache::tags($tag)->get(cache_url())) {
            $articleTags = $query->frontend($articleCategory)->get();

            Cache::tags($tag)->put(cache_url(), $articleTags, config('api.cache_minutes'));
        }

        if (!$articleTags) {
            return $this->failResponse('Article tags not found.');
        }

        return $this->successResponse($articleTags);
    }
}