<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Api\Controller;
use App\Queries\PageQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PageController extends Controller
{
    public function get(PageQuery $query, Request $request)
    {
        $tag = customer('domain') . '.page';

        if (!$page = Cache::tags($tag)->get(cache_url())) {
            $page = $query->frontend()->get();

            Cache::tags($tag)->put(cache_url(), $page, config('api.cache_minutes'));
        }

        if (!$page) {
            return $this->failResponse('Page not found.');
        }

        return $this->successResponse($page);
    }
}