<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\Product;
use App\Queries\ProductQuery;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function get(ProductQuery $query, Request $request)
    {
        $products = $query->get();

        if (!$products) {
            return $this->failResponse('Products not found.');
        }

        return $this->successResponse($products);
    }

    public function status($id, Request $request)
    {
        $product = Product::find($id);

        if (!isset($request->status) || !$product) {
            return $this->failResponse('Error');
        }

        $product->status = $request->status;
        $product->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}