<?php


namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function get(Request $request)
    {
        $users = User::all();

        if (!$users) {
            return $this->failResponse('Users not found.');
        }

        return $this->successResponse($users);
    }
}