<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\Media;
use App\Queries\MediaQuery;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function get(MediaQuery $query, Request $request)
    {
        $medias = $query->get();

        if (!$medias) {
            return $this->failResponse('Medias not found.');
        }

        return $this->successResponse($medias);
    }

    public function status($id, Request $request)
    {
        $article = Media::find($id);

        if (!isset($request->status) || !$article) {
            return $this->failResponse('Error');
        }

        $article->status = $request->status;
        $article->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}