<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\ArticleTag;
use App\Queries\ArticleTagQuery;
use Illuminate\Http\Request;

class ArticleTagController extends Controller
{
    public function get(ArticleTagQuery $query, Request $request)
    {
        $tags = $query->get();

        if (!$tags) {
            return $this->failResponse('Tags not found.');
        }

        return $this->successResponse($tags);
    }

    public function status($id, Request $request)
    {
        $tag = ArticleTag::find($id);

        if (!isset($request->status) || !$tag) {
            return $this->failResponse('Error');
        }

        $tag->status = $request->status;
        $tag->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}