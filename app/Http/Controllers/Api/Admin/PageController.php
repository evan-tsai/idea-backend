<?php


namespace App\Http\Controllers\Api\Admin;



use App\Http\Controllers\Api\Controller;
use App\Models\ArticleCategory;
use App\Models\Customer;
use App\Models\Page;
use App\Models\ProductTagCategory;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function get(Customer $customer, Request $request)
    {
        $pages = Page::where('customer_id', $customer->id)->get();

        if (!$pages) {
            return $this->failResponse('Pages not found.');
        }

        return $this->successResponse($pages);
    }

    public function status(Customer $customer, $id, Request $request)
    {
        $pages = Page::find($id);

        if (!isset($request->status) || !$pages) {
            return $this->failResponse('Error');
        }

        $pages->status = $request->status;
        $pages->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}