<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\Content;
use App\Queries\ContentQuery;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function get(ContentQuery $query, Request $request)
    {
        $contents = $query->get();

        if (!$contents) {
            return $this->failResponse('Contents not found.');
        }

        return $this->successResponse($contents);
    }

    public function status($id, Request $request)
    {
        $article = Content::find($id);

        if (!isset($request->status) || !$article) {
            return $this->failResponse('Error');
        }

        $article->status = $request->status;
        $article->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}