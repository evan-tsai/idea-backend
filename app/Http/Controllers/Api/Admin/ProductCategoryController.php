<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\ProductCategory;
use App\Queries\ProductCategoryQuery;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function get(ProductCategoryQuery $query, Request $request)
    {
        $categories = $query->tree();

        if (!$categories) {
            return $this->failResponse('Categories not found.');
        }

        return $this->successResponse($categories);
    }

    public function status($id, Request $request)
    {
        $product = ProductCategory::find($id);

        if (!isset($request->status) || !$product) {
            return $this->failResponse('Error');
        }

        $product->status = $request->status;
        $product->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}