<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function get(Request $request)
    {
        $customers = Customer::all();

        if (!$customers) {
            return $this->failResponse('Customers not found.');
        }

        return $this->successResponse($customers);
    }
}