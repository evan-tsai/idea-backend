<?php


namespace App\Http\Controllers\Api\Admin;



use App\Http\Controllers\Api\Controller;
use App\Models\Customer;
use App\Models\MediaType;
use Illuminate\Http\Request;

class MediaTypeController extends Controller
{
    public function get(Customer $customer, Request $request)
    {
        $types = MediaType::where('customer_id', $customer->id)->get();

        if (!$types) {
            return $this->failResponse('Types not found.');
        }

        return $this->successResponse($types);
    }

    public function status(Customer $customer, $id, Request $request)
    {
        $category = MediaType::find($id);

        if (!isset($request->status) || !$category) {
            return $this->failResponse('Error');
        }

        $category->status = $request->status;
        $category->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}