<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\Email;
use App\Queries\EmailQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmailController extends Controller
{
    public function get(EmailQuery $query, Request $request)
    {
        $emails = $query->get();

        if (!$emails) {
            return $this->failResponse('Emails not found.');
        }

        return $this->successResponse($emails);
    }

    public function read($id)
    {
        $email = Email::find($id);

        if (!$email) {
            return $this->failResponse('Error');
        }

        $email->read = 1;
        $email->save();

        return $this->successResponse('Success');
    }

    public function readAll()
    {
        DB::table('emails')
            ->where('customer_id', customer('id'))
            ->where('read', 0)
            ->update(['read' => 1]);

        return $this->successResponse([
            'type' => 'success',
            'text' => '全部已讀',
        ]);
    }
}