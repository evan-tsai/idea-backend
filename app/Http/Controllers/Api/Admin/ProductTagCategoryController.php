<?php


namespace App\Http\Controllers\Api\Admin;



use App\Http\Controllers\Api\Controller;
use App\Models\Customer;
use App\Models\ProductTagCategory;
use Illuminate\Http\Request;

class ProductTagCategoryController extends Controller
{
    public function get(Customer $customer, Request $request)
    {
        $categories = ProductTagCategory::where('customer_id', $customer->id)->get();

        if (!$categories) {
            return $this->failResponse('Categories not found.');
        }

        return $this->successResponse($categories);
    }

    public function status(Customer $customer, $id, Request $request)
    {
        $category = ProductTagCategory::find($id);

        if (!isset($request->status) || !$category) {
            return $this->failResponse('Error');
        }

        $category->status = $request->status;
        $category->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}