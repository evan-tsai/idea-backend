<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\Article;
use App\Queries\ArticleQuery;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function get(ArticleQuery $query, Request $request)
    {
        $articles = $query->get();

        if (!$articles) {
            return $this->failResponse('Articles not found.');
        }

        return $this->successResponse($articles);
    }

    public function status($id, Request $request)
    {
        $article = Article::find($id);

        if (!isset($request->status) || !$article) {
            return $this->failResponse('Error');
        }

        $article->status = $request->status;
        $article->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}