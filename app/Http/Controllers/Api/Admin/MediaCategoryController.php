<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\Controller;
use App\Models\MediaCategory;
use App\Queries\MediaCategoryQuery;
use Illuminate\Http\Request;

class MediaCategoryController extends Controller
{
    public function get(MediaCategoryQuery $query, Request $request)
    {
        $categories = $query->get();

        if (!$categories) {
            return $this->failResponse('Categories not found.');
        }

        return $this->successResponse($categories);
    }

    public function status($id, Request $request)
    {
        $tag = MediaCategory::find($id);

        if (!isset($request->status) || !$tag) {
            return $this->failResponse('Error');
        }

        $tag->status = $request->status;
        $tag->save();

        return $this->successResponse([
            'type' => 'success',
            'text' => '狀態調整成功',
        ]);
    }
}