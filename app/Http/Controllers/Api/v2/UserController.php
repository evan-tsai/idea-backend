<?php


namespace App\Http\Controllers\Api\v2;


use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function get(Request $request)
    {
            return $this->failResponse('Users not found.');
    }
}