<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function jsonResponse(string $message = '', $data = null, int $httpCode = 200)
    {
        return response()->json([
            'message' => $message,
            'data'    => $data,
        ], $httpCode);
    }

    protected function successResponse($data, string $message = 'ok')
    {
        return $this->jsonResponse($message, $data);
    }

    protected function failResponse(string $message, $data = null)
    {
        return $this->jsonResponse($message, $data, 422);
    }
}
