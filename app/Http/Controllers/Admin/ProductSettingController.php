<?php


namespace App\Http\Controllers\Admin;


use App\Models\Customer;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductSettingController
{
    public function index(Customer $customer)
    {
        return view('admin.product_setting.index', compact('customer'));
    }

    public function save(Request $request, Customer $customer)
    {
        DB::beginTransaction();
        try {
            $this->setSettings($request, $customer, 'product_category');
            $this->setSettings($request, $customer, 'product');
            $this->setSettings($request, $customer, 'product_tag');

            DB::commit();
            flash('存取成功')->success();

            return redirect()->route('product-setting.index', $customer);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    protected function setSettings($request, $customer, $type)
    {
        $settings = array_dot($request->{$type});
        foreach ($settings as $key => $item) {
            Setting::set($type . '.' . $key, $item, $customer->id);
        }
    }
}