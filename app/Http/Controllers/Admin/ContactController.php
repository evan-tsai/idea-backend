<?php


namespace App\Http\Controllers\Admin;


use App\Models\Customer;
use App\Models\Setting;
use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController
{
    public function index(Customer $customer)
    {
        $socials = Social::getSocialTypes()->map(function ($item) {
            return ['id' => $item, 'type' => $item];
        });

        $selectedSocials = Social::where([
            'customer_id' => $customer->id,
            'status' => 1,
        ])->get()->pluck('type');

        return view('admin.contact.index', compact('customer', 'socials', 'selectedSocials'));
    }

    public function save(Request $request, Customer $customer)
    {
        DB::beginTransaction();
        try {
            $this->setSettings($request, $customer, 'contact');
            $this->setSettings($request, $customer, 'email');
            $this->saveSocials($request->input('socials', []), $customer);

            DB::commit();
            flash('存取成功')->success();

            return redirect()->route('contact.index', $customer);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    protected function setSettings($request, $customer, $type)
    {
        $settings = array_dot($request->{$type});
        foreach ($settings as $key => $item) {
            Setting::set($type . '.' . $key, $item, $customer->id);
        }
    }

    protected function saveSocials($socials, $customer)
    {
        // Create or enable social
        foreach ($socials as $social) {
            $enabled = Social::firstOrCreate([
                'customer_id' => $customer->id,
                'type' => $social,
            ]);
            $enabled->status = 1;
            $enabled->save();
        }

        // Disable unselected socials
        $disabledSocials = Social::whereNotIn('type', $socials)->get();
        foreach ($disabledSocials as $disabled) {
            $disabled->status = 0;
            $disabled->save();
        }
    }
}