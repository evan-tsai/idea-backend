<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\MediaType;
use App\Writers\MediaTypeWriter;
use Illuminate\Http\Request;

class MediaTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'type', 'label' => '類型', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('admin.media_type.index', compact('customer', 'fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response

     */
    public function create(Customer $customer)
    {
        $mediaType = new MediaType;

        return view('admin.media_type.create', compact('customer', 'mediaType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Customer $customer)
    {
        $writer = new MediaTypeWriter($customer);
        $writer->store($request);

        flash('新增完成')->success();

        return redirect()->route('media-type.index', $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MediaType  $mediaType
     * @return \Illuminate\Http\Response
     */
    public function show(MediaType $mediaType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @param  \App\Models\MediaType $mediaType
     * @return \Illuminate\Http\Response

     */
    public function edit(Customer $customer, MediaType $mediaType)
    {
        return view('admin.media_type.edit', compact('customer', 'mediaType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @param  \App\Models\MediaType $mediaType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, MediaType $mediaType)
    {
        $writer = new MediaTypeWriter($customer);
        $writer->for($mediaType)->store($request);

        flash('新增完成')->success();

        return redirect()->route('media-type.index', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @param  \App\Models\MediaType $mediaType
     * @return \Illuminate\Http\Response

     */
    public function destroy(Customer $customer, MediaType $mediaType)
    {
        try {
            $mediaType->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('media-type.index', $customer));
    }
}
