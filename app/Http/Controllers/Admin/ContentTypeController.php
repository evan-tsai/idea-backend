<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContentType;
use App\Models\Customer;
use App\Writers\ContentTypeWriter;
use Illuminate\Http\Request;

class ContentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'type', 'label' => '類型', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('admin.content_type.index', compact('customer', 'fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response

     */
    public function create(Customer $customer)
    {
        $contentType = new ContentType;

        return view('admin.content_type.create', compact('customer', 'contentType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Customer $customer)
    {
        $writer = new ContentTypeWriter($customer);
        $writer->store($request);

        flash('新增完成')->success();

        return redirect()->route('content-type.index', $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContentType  $contentType
     * @return \Illuminate\Http\Response
     */
    public function show(ContentType $contentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @param  \App\Models\ContentType $contentType
     * @return \Illuminate\Http\Response

     */
    public function edit(Customer $customer, ContentType $contentType)
    {
        return view('admin.content_type.edit', compact('customer', 'contentType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @param  \App\Models\ContentType $contentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, ContentType $contentType)
    {
        $writer = new ContentTypeWriter($customer);
        $writer->for($contentType)->store($request);

        flash('新增完成')->success();

        return redirect()->route('content-type.index', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @param  \App\Models\ContentType $contentType
     * @return \Illuminate\Http\Response

     */
    public function destroy(Customer $customer, ContentType $contentType)
    {
        try {
            $contentType->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('content-type.index', $customer));
    }
}
