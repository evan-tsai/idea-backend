<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ArticleCategory;
use App\Models\Customer;
use App\Writers\ArticleCategoryWriter;
use Illuminate\Http\Request;

class ArticleCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'type', 'label' => '類型', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('admin.article_category.index', compact('customer', 'fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response

     */
    public function create(Customer $customer)
    {
        $articleCategory = new ArticleCategory;

        return view('admin.article_category.create', compact('customer', 'articleCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Customer $customer)
    {
        $writer = new ArticleCategoryWriter($customer);
        $writer->store($request);

        flash('新增完成')->success();

        return redirect()->route('article-category.index', $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleCategory $articleCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @param  \App\Models\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response

     */
    public function edit(Customer $customer, ArticleCategory $articleCategory)
    {
        return view('admin.article_category.edit', compact('customer', 'articleCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @param  \App\Models\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, ArticleCategory $articleCategory)
    {
        $writer = new ArticleCategoryWriter($customer);
        $writer->for($articleCategory)->store($request);

        flash('新增完成')->success();

        return redirect()->route('article-category.index', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @param  \App\Models\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response

     */
    public function destroy(Customer $customer, ArticleCategory $articleCategory)
    {
        try {
            $articleCategory->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('article-category.index', $customer));
    }
}
