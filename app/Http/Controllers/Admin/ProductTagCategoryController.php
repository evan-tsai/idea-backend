<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\ProductTagCategory;
use App\Writers\ProductTagCategoryWriter;
use Illuminate\Http\Request;

class ProductTagCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'type', 'label' => '類型', 'sortable' => 'true'],
            ['key' => 'multiple', 'label' => '多選', 'formatter' => 'formatBoolean', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('admin.product_tag_category.index', compact('customer', 'fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response

     */
    public function create(Customer $customer)
    {
        $productTagCategory = new ProductTagCategory;

        return view('admin.product_tag_category.create', compact('customer', 'productTagCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Customer $customer)
    {
        $writer = new ProductTagCategoryWriter($customer);
        $writer->store($request);

        flash('新增完成')->success();

        return redirect()->route('product-tag-category.index', $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductTagCategory  $productTagCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductTagCategory $productTagCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @param  \App\Models\ProductTagCategory $productTagCategory
     * @return \Illuminate\Http\Response

     */
    public function edit(Customer $customer, ProductTagCategory $productTagCategory)
    {
        return view('admin.product_tag_category.edit', compact('customer', 'productTagCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @param  \App\Models\ProductTagCategory $productTagCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, ProductTagCategory $productTagCategory)
    {
        $writer = new ProductTagCategoryWriter($customer);
        $writer->for($productTagCategory)->store($request);

        flash('新增完成')->success();

        return redirect()->route('product-tag-category.index', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @param  \App\Models\ProductTagCategory $productTagCategory
     * @return \Illuminate\Http\Response

     */
    public function destroy(Customer $customer, ProductTagCategory $productTagCategory)
    {
        try {
            $productTagCategory->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('product-tag-category.index', $customer));
    }
}
