<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Page;
use App\Writers\PageWriter;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'type', 'label' => '類型', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('admin.page.index', compact('customer', 'fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response

     */
    public function create(Customer $customer)
    {
        $page = new Page;

        return view('admin.page.create', compact('customer', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Customer $customer)
    {
        $writer = new PageWriter($customer);
        $writer->store($request);

        flash('新增完成')->success();

        return redirect()->route('page.index', $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response

     */
    public function edit(Customer $customer, Page $page)
    {
        return view('admin.page.edit', compact('customer', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, Page $page)
    {
        $writer = new PageWriter($customer);
        $writer->for($page)->store($request);

        flash('新增完成')->success();

        return redirect()->route('page.index', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response

     */
    public function destroy(Customer $customer, Page $page)
    {
        try {
            $page->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('page.index', $customer));
    }
}
