<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function jsonNotify(string $type, string $text)
    {
        $code = $type === 'success' ? 200 : 422;
        $payload = compact('type', 'text');

        return response()->json($payload, $code);
    }

    protected function notifyAndRedirect(string $type, string $text, string $redirect = null)
    {
        if (request()->wantsJson()) {
            return $this->jsonNotify($type, $text);
        }

        flash($text)->$type();

        if ($redirect) {
            return redirect($redirect);
        }

        return $this;
    }
}
