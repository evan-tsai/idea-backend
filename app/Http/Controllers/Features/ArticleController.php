<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Queries\ArticleTagQuery;
use App\Writers\ArticleWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function index(ArticleCategory $articleCategory)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'trans.name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('features.article.index', compact('fields', 'articleCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function create(ArticleCategory $articleCategory)
    {
        $article = new Article;

        $tags = resolve(ArticleTagQuery::class)->byType($articleCategory->type)->get();

        return view('features.article.create', compact('article', 'articleCategory', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ArticleWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, ArticleWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('article.index', $request->article_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ArticleCategory $articleCategory
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleCategory $articleCategory, Article $article)
    {
        $tags = resolve(ArticleTagQuery::class)->byType($articleCategory->type)->get();

        return view('features.article.edit', compact('article', 'articleCategory', 'tags'));
    }

    /**
     * Show the form for copying the specified resource.
     *
     * @param ArticleCategory $articleCategory
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function copy(ArticleCategory $articleCategory, Article $article)
    {
        $article->exists = false;
        $article->slug = null;

        $tags = resolve(ArticleTagQuery::class)->byType($articleCategory->type)->get();

        return view('features.article.create', compact('article', 'articleCategory', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ArticleWriter $writer
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, ArticleWriter $writer, Article $article)
    {
        DB::beginTransaction();
        try {
            $writer->for($article)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('article.index', $request->article_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ArticleCategory $articleCategory
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleCategory $articleCategory, Article $article)
    {
        try {
            $article->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('article.index', $articleCategory));
    }
}
