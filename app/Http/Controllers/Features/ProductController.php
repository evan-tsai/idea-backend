<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\Product;
use App\Queries\ProductCategoryQuery;
use App\Queries\ProductQuery;
use App\Queries\ProductTagCategoryQuery;
use App\Writers\ProductWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Product::query()->paginate(2);
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'category.trans.name', 'label' => '類別', 'sortable' => 'true'],
            ['key' => 'trans.name', 'label' => '名稱', 'sortable' => 'true'],
        ]);

        if (setting('product.fields.model')) {
            $fields->push(['key' => 'model_name', 'label' => '型號', 'sortable' => 'true']);
        }

        if (setting('product.fields.stock')) {
            $fields->push(['key' => 'stock', 'label' => '庫存', 'sortable' => 'true']);
        }

        $fields->push(['key' => 'status', 'label' => '狀態', 'sortable' => 'true']);

        $categories = resolve(ProductCategoryQuery::class)->tree();

        return view('features.product.index', compact('fields', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product;

        $categories = resolve(ProductCategoryQuery::class)->tree();

        $tagTypes = resolve(ProductTagCategoryQuery::class)->get();

        $galleries = collect();

        return view('features.product.create', compact('product', 'categories', 'tagTypes', 'galleries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, ProductWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('product.index');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = resolve(ProductCategoryQuery::class)->tree();

        $tagTypes = resolve(ProductTagCategoryQuery::class)->get();

        $galleries = resolve(ProductQuery::class)->collections($product, Collection::TYPE_GALLERY);

        return view('features.product.edit', compact('product', 'categories', 'tagTypes', 'galleries'));
    }

    /**
     * Show the form for copying the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function copy(Product $product)
    {
        $product->exists = false;
        $product->slug = null;

        $categories = resolve(ProductCategoryQuery::class)->tree();

        $tagTypes = resolve(ProductTagCategoryQuery::class)->get();

        $galleries = resolve(ProductQuery::class)->collections($product, Collection::TYPE_GALLERY);

        return view('features.product.create', compact('product', 'categories', 'tagTypes', 'galleries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductWriter $writer
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, ProductWriter $writer, Product $product)
    {
        DB::beginTransaction();
        try {
            $writer->for($product)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('product.index');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('product.index'));
    }
}
