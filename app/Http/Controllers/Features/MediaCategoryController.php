<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\MediaCategory;
use App\Models\MediaType;
use App\Writers\MediaCategoryWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MediaCategoryController extends Controller
{
    public function index(MediaType $mediaType)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'trans.name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('features.media_category.index', compact('fields', 'mediaType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MediaType $mediaType
     * @return \Illuminate\Http\Response
     */
    public function create(MediaType $mediaType)
    {
        $mediaCategory = new MediaCategory;

        return view('features.media_category.create', compact('mediaType', 'mediaCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MediaCategoryWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, MediaCategoryWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('media-category.index', $request->media_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MediaCategory  $mediaCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MediaCategory $mediaCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MediaType $mediaType
     * @param  \App\Models\MediaCategory $mediaCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(MediaType $mediaType, MediaCategory $mediaCategory)
    {
        return view('features.media_category.edit', compact('mediaType', 'mediaCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MediaCategory  $mediaCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MediaCategoryWriter $writer, MediaCategory $mediaCategory)
    {
        DB::beginTransaction();
        try {
            $writer->for($mediaCategory)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('media-category.index', $request->media_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MediaType $mediaType
     * @param  \App\Models\MediaCategory $mediaCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MediaType $mediaType, MediaCategory $mediaCategory)
    {
        try {
            $mediaCategory->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('media-category.index', $mediaType));
    }
}
