<?php

namespace App\Http\Controllers\Features;

use App\Models\ArticleCategory;
use App\Models\ArticleTag;
use App\Http\Controllers\Controller;
use App\Writers\ArticleTagWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleTagController extends Controller
{
    public function index(ArticleCategory $articleCategory)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'trans.name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('features.article_tag.index', compact('fields', 'articleCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function create(ArticleCategory $articleCategory)
    {
        $articleTag = new ArticleTag;

        return view('features.article_tag.create', compact('articleTag', 'articleCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ArticleTagWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, ArticleTagWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('article-tag.index', $request->article_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArticleTag  $articleTag
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleTag $articleTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ArticleCategory $articleCategory
     * @param  \App\Models\ArticleTag $articleTag
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleCategory $articleCategory, ArticleTag $articleTag)
    {
        return view('features.article_tag.edit', compact('articleTag', 'articleCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArticleTag  $articleTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticleTagWriter $writer, ArticleTag $articleTag)
    {
        DB::beginTransaction();
        try {
            $writer->for($articleTag)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('article-tag.index', $request->article_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ArticleCategory $articleCategory
     * @param  \App\Models\ArticleTag $articleTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleCategory $articleCategory, ArticleTag $articleTag)
    {
        try {
            $articleTag->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('article-tag.index', $articleCategory));
    }
}
