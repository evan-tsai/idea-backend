<?php


namespace App\Http\Controllers\Features;


use App\Http\Controllers\Controller;
use App\Models\ContactInfo;
use App\Models\Setting;
use App\Models\Social;
use App\Queries\SocialQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index()
    {
        $contactInfo = new ContactInfo;
        $socials = resolve(SocialQuery::class)->get();

        return view('features.contact.index', compact('contactInfo', 'socials'));
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->saveContactInfo($request);
            $this->saveSocials($request->input('socials'));
            $this->setSettings($request, 'email');

            DB::commit();
            flash('存檔成功', 'success');
            return redirect()->route('customer-contact.index');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    protected function saveContactInfo($request)
    {
        $customerId = customer('id');
        foreach (customer('languages') as $language) {
            $contactInfo = ContactInfo::firstOrNew([
                'customer_id' => $customerId,
                'lang' => $language->id
            ]);

            $contactInfo->phone = $request->input('phone.' . $language->id);
            $contactInfo->phone2 = $request->input('phone2.' . $language->id);
            $contactInfo->fax = $request->input('fax.' . $language->id);
            $contactInfo->fax2 = $request->input('fax2.' . $language->id);
            $contactInfo->email = $request->input('email.' . $language->id);
            $contactInfo->address = $request->input('address.' . $language->id);

            $contactInfo->save();
        }
    }

    protected function saveSocials($socials)
    {
        if (empty($socials)) return null;

        $customerId = customer('id');

        foreach ($socials as $key => $social) {
            if ($model = Social::where('type', $key)->where('customer_id', $customerId)->first()) {
                $model->value = $social;
                $model->save();
            }
        }
    }

    protected function setSettings($request, $type)
    {
        $settings = array_dot($request->{$type});
        foreach ($settings as $key => $item) {
            Setting::set($type . '.' . $key, $item, customer('id'));
        }
    }
}