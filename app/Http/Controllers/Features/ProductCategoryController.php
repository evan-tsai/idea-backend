<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Queries\ProductCategoryQuery;
use App\Writers\ProductCategoryWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('features.product_category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productCategory = new ProductCategory;

        $tree = resolve(ProductCategoryQuery::class)->tree();

        return view('features.product_category.create', compact('productCategory', 'tree'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductCategoryWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, ProductCategoryWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('product-category.index');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        $tree = resolve(ProductCategoryQuery::class)->tree();

        return view('features.product_category.edit', compact('productCategory', 'tree'));
    }

    /**
     * Show the form for copying the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function copy(ProductCategory $productCategory)
    {
        $productCategory->exists = false;
        $productCategory->slug = null;

        $tree = resolve(ProductCategoryQuery::class)->tree();

        return view('features.product_category.create', compact('productCategory', 'tree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductCategoryWriter $writer
     * @param  \App\Models\ProductCategory $productCategory
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, ProductCategoryWriter $writer, ProductCategory $productCategory)
    {
        DB::beginTransaction();
        try {
            $writer->for($productCategory)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('product-category.index');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        try {
            $productCategory->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('product-category.index'));
    }
}
