<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\MediaType;
use App\Queries\MediaCategoryQuery;
use App\Writers\MediaWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MediaItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param MediaType $mediaType
     * @return \Illuminate\Http\Response
     */
    public function index(MediaType $mediaType)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'trans.name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('features.media.index', compact('fields', 'mediaType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MediaType $mediaType
     * @return \Illuminate\Http\Response
     */
    public function create(MediaType $mediaType)
    {
        $media = new Media;

        $categories = resolve(MediaCategoryQuery::class)->byType($mediaType->type)->get();

        return view('features.media.create', compact('media', 'mediaType', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MediaWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, MediaWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('media.index', $request->media_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MediaType $mediaType
     * @param  \App\Models\Media $media
     * @return \Illuminate\Http\Response
     */
    public function edit(MediaType $mediaType, Media $media)
    {
        $categories = resolve(MediaCategoryQuery::class)->byType($mediaType->type)->get();

        return view('features.media.edit', compact('media', 'mediaType', 'categories'));
    }

    /**
     * Show the form for copying the specified resource.
     *
     * @param MediaType $mediaType
     * @param  \App\Models\Media $media
     * @return \Illuminate\Http\Response
     */
    public function copy(MediaType $mediaType, Media $media)
    {
        $media->exists = false;

        $categories = resolve(MediaCategoryQuery::class)->byType($mediaType->type)->get();

        return view('features.media.create', compact('media', 'mediaType', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MediaWriter $writer
     * @param  \App\Models\Media $media
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, MediaWriter $writer, Media $media)
    {
        DB::beginTransaction();
        try {
            $writer->for($media)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('media.index', $request->media_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MediaType $mediaType
     * @param  \App\Models\Media $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(MediaType $mediaType, Media $media)
    {
        try {
            $media->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('media.index', $mediaType));
    }
}
