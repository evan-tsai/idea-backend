<?php


namespace App\Http\Controllers\Features;


use App\Http\Controllers\Controller;
use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmailController extends Controller
{
    public function index()
    {
        $fields = collect([
            ['key' => 'type', 'label' => '類型', 'sortable' => 'true'],
            ['key' => 'fields.email', 'label' => '信箱', 'sortable' => 'true'],
            ['key' => 'read', 'label' => '狀態', 'sortable' => 'true'],
            ['key' => 'created_at', 'label' => '時間', 'sortable' => 'true'],
        ]);

        return view('features.mail.index', compact('fields'));
    }

    public function download(Request $request)
    {
        $file = $request->file;

        if (!$file) return null;

        $pathInfo = pathinfo($file);
        $extension = $pathInfo['extension'];
        $fileName = $pathInfo['filename'] . '.' . $extension;
        $headers = ['Content-Type' => $extension];

        return Storage::disk('s3')->download($file, $fileName, $headers);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Email $email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $email)
    {
        try {
            $email->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('email.index'));
    }
}