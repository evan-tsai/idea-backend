<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\ContentType;
use App\Writers\ContentWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ContentType $contentType
     * @return \Illuminate\Http\Response
     */
    public function index(ContentType $contentType)
    {
        $fields = collect([
            ['key' => 'id', 'label' => 'ID', 'sortable' => 'true'],
            ['key' => 'trans.name', 'label' => '名稱', 'sortable' => 'true'],
            ['key' => 'status', 'label' => '狀態', 'sortable' => 'true'],
        ]);

        return view('features.content.index', compact('fields', 'contentType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ContentType $contentType
     * @return \Illuminate\Http\Response
     */
    public function create(ContentType $contentType)
    {
        $content = new Content;

        return view('features.content.create', compact('content', 'contentType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ContentWriter $writer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, ContentWriter $writer)
    {
        DB::beginTransaction();
        try {
            $writer->store($request);

            DB::commit();
            flash('新增完成', 'success');
            return redirect()->route('content.index', $request->content_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ContentType $contentType
     * @param  \App\Models\Content $content
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentType $contentType, Content $content)
    {
        return view('features.content.edit', compact('content', 'contentType'));
    }

    /**
     * Show the form for copying the specified resource.
     *
     * @param ContentType $contentType
     * @param  \App\Models\Content $content
     * @return \Illuminate\Http\Response
     */
    public function copy(ContentType $contentType, Content $content)
    {
        $content->exists = false;

        return view('features.content.create', compact('content', 'contentType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ContentWriter $writer
     * @param  \App\Models\Content $content
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, ContentWriter $writer, Content $content)
    {
        DB::beginTransaction();
        try {
            $writer->for($content)->update($request);

            DB::commit();
            flash('修改完成', 'success');
            return redirect()->route('content.index', $request->content_type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ContentType $contentType
     * @param  \App\Models\Content $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentType $contentType, Content $content)
    {
        try {
            $content->delete();

            $type = 'success';
            $text = '刪除成功';
        } catch (\Exception $e) {
            $type = 'error';
            $text = $e->getMessage();
        }

        return $this->notifyAndRedirect($type, $text, route('content.index', $contentType));
    }
}
