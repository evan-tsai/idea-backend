<?php


namespace App\Http\Controllers\Features;


use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\Page;
use App\Queries\PageQuery;
use App\Writers\PageWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function index(Page $page)
    {
        $banners = resolve(PageQuery::class)->collections($page, Collection::TYPE_BANNER);
        $gallery = resolve(PageQuery::class)->collections($page, Collection::TYPE_GALLERY);

        return view('features.page.index', compact('page', 'banners', 'gallery'));
    }

    public function save(Request $request, PageWriter $writer, Page $page)
    {
        DB::beginTransaction();
        try {
            $writer->for($page)->save($request);

            DB::commit();
            flash('存檔成功', 'success');
            return redirect()->route('customer-page.index', $page->type);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}