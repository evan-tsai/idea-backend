<?php

function languages()
{
    return resolve('languages');
}

function customers()
{
    return resolve('customers');
}

function customer(string $attribute = null)
{
    $customer = request()->realDomainCustomer;

    return $attribute ? optional($customer)->{$attribute} : $customer;
}

function isAdminDomain($domain = null)
{
    $domain = $domain ? $domain : customer('domain');
    return $domain === config('admin.domain');
}

function isSuperAdmin()
{
    return auth()->check() && auth()->user()->name() === config('admin.username');
}

function setting($key, $customerId = null, $default = null, $prefix = null)
{
    // Append prefix to key
    if ($prefix) {
        $array = explode('.', $key);
        array_splice($array, 1, 0, $prefix);
        $key = implode('.', $array);
    }

    if ($customerId) {
        return \App\Models\Setting::get($key, $customerId, $default);
    }

    if (!$id = customer('id')) {
        throw new \RuntimeException('Customer ID cannot be found.');
    }

    if ($value = \App\Models\Setting::get($key, $id)) {
        return $value;
    }

    return null;
}

function setting_array($key)
{
    if (!$id = customer('id')) {
        throw new \RuntimeException('Customer ID cannot be found.');
    }

    if ($array = \App\Models\Setting::getArray($key, $id)) {
        return $array;
    }

    return [];
}

function cache_url()
{
    $url = request()->url();
    $queryParams = request()->query();
    ksort($queryParams);
    $queryString = http_build_query($queryParams);

    return "{$url}?{$queryString}";
}

function editorJsonContentEncode($editors)
{
    return htmlspecialchars(json_encode($editors), ENT_QUOTES, 'UTF-8');
}