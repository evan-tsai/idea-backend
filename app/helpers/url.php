<?php

function app_domain()
{
    return parse_url(config('app.url'), PHP_URL_HOST);
}