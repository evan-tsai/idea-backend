<?php


namespace App\Queries;


use App\Models\Product;
use App\Queries\Traits\HasCollections;

class ProductQuery extends BaseQuery
{
    use HasCollections;

    protected function query()
    {
        return Product::query()->with(['category']);
    }

    public function frontend()
    {
        $this->active()->order()->withCollections()->with(['translations.editors', 'tags', 'tags.category']);

        $category = request()->input('category');

        if ($category) {
            $this->query->whereHas('category', function ($query) use ($category) {
                $query->where('id', $category);
                $query->orWhere('slug', $category);
            });
        }

        $tag = request()->input('tag');

        if ($tag) {
            $this->query->whereHas('tags', function ($query) use ($tag) {
                $query->where('id', $tag);
            });
        }

        $keywords = request()->input('keywords');

        if ($keywords) {
            $this->query->whereHas('translations', function ($query) use ($keywords) {
                $query->where('name', 'like', '%' . $keywords . '%');
            })
            ->orWhereHas('translations.editors', function ($query) use ($keywords) {
                $query->where('content', 'like', '%' . $keywords . '%');
            });
        }

        return $this;
    }
}