<?php


namespace App\Queries;


use App\Models\ProductCategory;
use App\Queries\Traits\HasMultiLevels;

class ProductCategoryQuery extends BaseQuery
{
    use HasMultiLevels;

    protected function query()
    {
        return ProductCategory::query();
    }

    public function frontend()
    {
        $this->active()->order()->with(['products', 'products.translations.collections', 'products.translations.editors']);

        return $this;
    }
}