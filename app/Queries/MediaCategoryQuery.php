<?php


namespace App\Queries;


use App\Models\MediaCategory;
use App\Queries\Traits\HasType;

class MediaCategoryQuery extends BaseQuery
{
    use HasType;

    protected function query()
    {
        return MediaCategory::query()->with(['type'])
            ->whereHas('type', function ($query) {
                $query->where('status', 1);
            });
    }

    public function frontend()
    {
        $this->active()->order()->with(['media']);

        return $this;
    }
}