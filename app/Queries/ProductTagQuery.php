<?php


namespace App\Queries;


use App\Models\ProductTag;

class ProductTagQuery extends BaseQuery
{
    protected function query()
    {
        return ProductTag::query()->with(['category'])
            ->whereHas('category', function ($query) {
                $query->where('status', 1);
            });
    }

    public function frontend()
    {
        $this->active()->order();

        $category = request()->input('category');

        if ($category) {
            $this->query->whereHas('category', function ($query) use ($category) {
                $query->where('id', $category);
                $query->orWhere('type', $category);
            });
        }

        $productCategory = request()->input('product_category');

        if ($productCategory) {
            $this->query->whereHas('products', function ($query) use ($productCategory) {
                $query->whereHas('category', function ($query) use ($productCategory) {
                    $query->where('id', $productCategory);
                    $query->orWhere('slug', $productCategory);
                });
            });
        }

        return $this;
    }
}