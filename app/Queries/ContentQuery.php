<?php


namespace App\Queries;


use App\Models\Content;
use App\Queries\Traits\HasType;

class ContentQuery extends BaseQuery
{
    use HasType;

    protected function query()
    {
        return Content::query();
    }

    public function frontend()
    {
        $this->active()->order()->with('translations.editors');

        return $this;
    }
}