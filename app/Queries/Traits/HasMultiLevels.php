<?php


namespace App\Queries\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait HasMultiLevels
{
    public function tree()
    {
        $allCategories = $this->get();

        $categoryTree = collect();

        foreach ($allCategories->where('parent_id', 0) as $rootCategory) {
            $tree = $this->findChildren($allCategories, $rootCategory);
            $categoryTree->push($tree);
        }

        return $categoryTree;
    }

    protected function findChildren(Collection $allCategories, Model $category)
    {
        $children = $allCategories->where('parent_id', $category->id);
        $childrenCategories = collect();

        if ($children->count()) {
            foreach ($children as $child) {
                $childCategory = $this->findChildren($allCategories, $child);
                if (!$childCategory) {
                    continue;
                }

                $childrenCategories->push($childCategory);
            }
        }

        return collect([
            'category'       => $category,
            'children'       => $childrenCategories,
            'children_count' => $childrenCategories->count(),
        ]);
    }
}