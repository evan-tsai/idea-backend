<?php


namespace App\Queries\Traits;


trait HasCollections
{
    public function collections($model, $type)
    {
        return $model->translations->mapWithKeys(function ($item) use ($type) {
            return [$item['lang'] => $item->collections->where('type', $type)];
        });
    }

    protected function withCollections()
    {
        $this->query->with(['translations.collections' => function ($query) {
            $query->where('status', 1);
        }]);

        return $this;
    }
}