<?php


namespace App\Queries\Traits;


trait HasType
{
    public function byType($type, $typeRelation = 'type')
    {
        $this->query->whereHas($typeRelation, function ($query) use ($type) {
            $query->where('type', $type);
        });

        return $this;
    }
}