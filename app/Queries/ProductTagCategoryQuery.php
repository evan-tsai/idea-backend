<?php


namespace App\Queries;


use App\Models\ProductTagCategory;

class ProductTagCategoryQuery extends BaseQuery
{
    protected function query()
    {
        return ProductTagCategory::query()
            ->where('customer_id', customer('id'))
            ->where('status', 1);
    }
}