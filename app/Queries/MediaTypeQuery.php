<?php


namespace App\Queries;


use App\Models\MediaType;

class MediaTypeQuery extends BaseQuery
{
    protected function query()
    {
        return MediaType::query()
            ->where('customer_id', customer('id'))
            ->where('status', 1);
    }
}