<?php


namespace App\Queries;


use App\Models\Social;

class SocialQuery extends BaseQuery
{
    protected function query()
    {
        return Social::query()
            ->where('customer_id', customer('id'))
            ->where('status', 1);
    }

    public function frontend()
    {
        return $this;
    }
}