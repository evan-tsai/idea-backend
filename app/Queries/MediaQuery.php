<?php


namespace App\Queries;


use App\Models\Media;
use App\Queries\Traits\HasType;

class MediaQuery extends BaseQuery
{
    use HasType;

    protected function query()
    {
        return Media::query()->with(['category']);
    }

    public function frontend($mediaType)
    {
        $this->active()->order()->where('type_id', $mediaType->id);;

        return $this;
    }
}