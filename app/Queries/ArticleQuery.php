<?php


namespace App\Queries;


use App\Models\Article;
use App\Queries\Traits\HasType;

class ArticleQuery extends BaseQuery
{
    use HasType;

    protected function query()
    {
        return Article::query()->with(['category']);
    }

    public function frontend()
    {
        $now = now();

        $this->active()
            ->orderBy('publish_start', 'desc')
            ->with(['translations.editors', 'tags'])
            ->where('publish_start', '<=', $now)
            ->where(function ($query) use ($now) {
                $query->where('publish_end', '>=', $now)->orWhereNull('publish_end');
            });

        return $this;
    }
}