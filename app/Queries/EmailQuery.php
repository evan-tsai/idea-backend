<?php


namespace App\Queries;


use App\Models\Email;

class EmailQuery extends BaseQuery
{
    protected function query()
    {
        return Email::query()->latest();
    }
}