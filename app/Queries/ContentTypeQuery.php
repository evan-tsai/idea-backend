<?php


namespace App\Queries;


use App\Models\ContentType;

class ContentTypeQuery extends BaseQuery
{
    protected function query()
    {
        return ContentType::query()
            ->where('customer_id', customer('id'))
            ->where('status', 1);
    }
}