<?php

namespace App\Queries;


abstract class BaseQuery
{
    protected $query;
    protected $filters = [];

    public function __construct()
    {
        $this->query = $this->query();
    }

    public function __call(string $method, array $parameters)
    {
        return $this->query->$method(...$parameters);
    }

    abstract protected function query();

    protected function applyFilters()
    {
        foreach (request()->input() as $name => $value) {
            if (is_null($value)) {
                continue;
            }

            switch ($name) {
                case 'created_start':
                case 'updated_start':
                    $this->query->where($name, '>=', $value);
                    break;
                case 'created_end':
                case 'updated_end':
                    $this->query->where($name, '<=', $value);
                    break;
                case 'id':
                case 'type':
                case 'slug':
                case 'status':
                case 'parent_id':
                case 'category_id':
                case 'type_id':
                    $this->query->where($name, '=', $value);
                    break;
            }
        }
    }

    protected function active()
    {
        $this->query->where('status', 1);

        return $this;
    }

    protected function order()
    {
        $this->query->orderBy('sort', 'asc');

        return $this;
    }

    public function get()
    {
        $this->applyFilters();

        if (request()->filled('id') || request()->filled('type') || request()->filled('slug')) {
            return $this->query->first();
        }

        // Paginate
        if ($limit = request()->input('limit')) {
            $pagination = $this->query->paginate($limit);
            return [
                'total' => $pagination->total(),
                'results' => $this->query->get(),
            ];
        }

        return $this->query->get();
    }
}
