<?php


namespace App\Queries;


use App\Models\ArticleTag;

class ArticleTagQuery extends BaseQuery
{
    protected function query()
    {
        return ArticleTag::query()->with(['category'])
            ->whereHas('category', function ($query) {
                $query->where('status', 1);
            });
    }

    public function byType($type)
    {
        $this->query->whereHas('category', function ($query) use ($type) {
            $query->where('type', $type);
        });

        return $this;
    }

    public function frontend($articleCategory)
    {
        $this->active()->order()->where('category_id', $articleCategory->id);

        return $this;
    }
}