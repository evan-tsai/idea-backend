<?php


namespace App\Queries;


use App\Models\ArticleCategory;

class ArticleCategoryQuery extends BaseQuery
{
    protected function query()
    {
        return ArticleCategory::query()
            ->where('customer_id', customer('id'))
            ->where('status', 1);
    }
}