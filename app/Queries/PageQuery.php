<?php


namespace App\Queries;


use App\Models\Page;
use App\Queries\Traits\HasCollections;

class PageQuery extends BaseQuery
{
    use HasCollections;

    protected function query()
    {
        return Page::query()
            ->where('customer_id', customer('id'))
            ->where('status', 1)
            ->orderBy('sort', 'asc');
    }

    public function frontend()
    {
        $this->withCollections()->with('translations.editors');

        return $this;
    }
}