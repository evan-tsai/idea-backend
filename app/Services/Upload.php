<?php


namespace App\Services;


use Illuminate\Support\Facades\Storage;

class Upload
{
    public static function uploadToS3($file, $folder)
    {
        if (!$file) return null;

        $disk = Storage::disk('s3');
        $originalName = $file->getClientOriginalName();
        $extension = strtolower($file->getClientOriginalExtension());
        $uploadedFileName = date('Y_m_d_His') . '_' . $originalName;
        $path = customer('domain') . '/' . $folder . '/' . $uploadedFileName;

        $disk->put($path, file_get_contents($file));
        return compact('extension', 'path', 'originalName');
    }
}