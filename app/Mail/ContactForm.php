<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class ContactForm extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    private $title;
    /**
     * @var array
     */
    private $data;
    private $file;

    /**
     * Create a new message instance.
     *
     * @param string $title
     * @param array $data
     * @param null $file
     */
    public function __construct(string $title, array $data, $file = null)
    {
        $this->title = $title;
        $this->data = $data;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->title;
        $data = $this->data;

        $message = $this->subject($title)->view('emails.contact', compact('title', 'data'));

        if ($this->file) {
            $file = Storage::disk('s3')->get($this->file['path']);
            $message = $message->attachData($file, $this->file['originalName']);
        }

        return $message;
    }
}
