<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    const TYPE_BANNER = 'banner';
    const TYPE_GALLERY = 'gallery';

    public $guarded = ['id'];

    public function products()
    {
        return $this->morphByMany('App\Models\ProductTranslation', 'collection_taggable');
    }
}
