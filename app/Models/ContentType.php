<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    protected $guarded = ['id'];

    /** relations */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Content', 'type_id', 'id')->withoutGlobalScopes();
    }
}
