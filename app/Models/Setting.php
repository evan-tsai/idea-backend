<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = [];

    public static function get($key, $customerId, $default = null)
    {
        if (self::has($key, $customerId)) {
            return self::getAllSettings()
                ->where('customer_id', $customerId)
                ->where('key', $key)
                ->first()
                ->value;
        }

        return $default;
    }

    public static function getArray($key, $customerId)
    {
        // reject = like statement, map with keys without parent array
        $data = self::getAllSettings()
            ->where('customer_id', $customerId)
            ->reject(function($element) use ($key) {
                return mb_strpos($element->key, $key . '.') === false;
            })
            ->mapWithKeys(function ($item) use ($key) {
                $removeParent = str_replace($key . '.', '', $item['key']);
                return [$removeParent => $item['value']];
            });

        // set dot notation to array
        $array = [];
        foreach ($data as $key => $value) {
            array_set($array, $key, $value);
        }

        return collect($array)->recursive();
    }

    public static function has($key, $customerId)
    {
        return (boolean) self::getAllSettings()
            ->where('customer_id', $customerId)
            ->where('key', $key)->count();
    }

    public static function set($key, $value, $customerId)
    {
        $setting = self::getAllSettings()
            ->where('customer_id', $customerId)
            ->where('key', $key)->first();

        if ($setting) {
            $setting->value = $value;
            return $setting->save() ? $value : false;
        }

        return self::add($key, $value, $customerId);
    }

    public static function add($key, $value, $customerId)
    {
        if (self::has($key, $customerId)) {
            return self::set($key, $value, $customerId);
        }

        return self::create(['customer_id' => $customerId, 'key' => $key, 'value' => $value]) ? $value : false;
    }

    public static function remove($key, $customerId)
    {
        if( self::has($key, $customerId) ) {
            return self::where([
                'customer_id' => $customerId,
                'key' => $key,
            ])->delete();
        }

        return false;
    }

    public static function getAllSettings()
    {
        return \Cache::rememberForever('settings.all', function() {
            return self::all();
        });
    }
}
