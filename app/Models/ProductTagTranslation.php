<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTagTranslation extends Model
{
    protected $guarded = ['id'];

    public function base()
    {
        return $this->belongsTo(ProductTag::class, 'base_id', 'id');
    }
}
