<?php

namespace App\Models\Traits;

trait MultiLevelCategory
{
    /** relations */
    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }
}
