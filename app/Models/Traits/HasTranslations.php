<?php


namespace App\Models\Traits;


trait HasTranslations
{
    public function translations()
    {
        $translationClass = __CLASS__ . 'Translation';
        if (!class_exists($translationClass)) {
            throw new \UnexpectedValueException($translationClass . ' is not a valid class');
        }

        return $this->hasMany($translationClass, 'base_id', 'id');
    }

    public function translateColumn($lang, $column)
    {
        return optional($this->getTranslations($lang))->{$column};
    }

    public function getTranslations($lang)
    {
        return $this->translations->where('lang', $lang)->first();
    }

    public function getTransAttribute()
    {
        return $this->getTranslations(request()->input('lang', customer('default_lang')));
    }
}