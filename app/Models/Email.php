<?php

namespace App\Models;

use App\Mail\ContactForm;
use App\Models\Scopes\CustomerScope;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $guarded = ['id'];

    protected $casts = ['fields' => 'array'];

    protected $appends = ['content'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getContentAttribute()
    {
        return (new ContactForm($this->title, $this->fields))->render();
    }
}
