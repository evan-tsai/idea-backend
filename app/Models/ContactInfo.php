<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use Illuminate\Database\Eloquent\Model;

class ContactInfo extends Model
{
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function translateColumn($lang, $column)
    {
        return optional($this->getTranslations($lang))->{$column};
    }

    public function getTranslations($lang)
    {
        return $this->query()->where('lang', $lang)->first();
    }
}
