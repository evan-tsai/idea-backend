<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = ['id'];

    public function languages()
    {
        return $this->belongsToMany(Language::class)->orderBy('sort');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
