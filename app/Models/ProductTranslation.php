<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    protected $guarded = ['id'];

    public function base()
    {
        return $this->belongsTo(Product::class, 'base_id', 'id');
    }

    public function editors()
    {
        return $this->morphMany('App\Models\Editor', 'editable')->orderBy('sort', 'asc');
    }

    public function collections()
    {
        return $this->morphToMany('App\Models\Collection', 'collection_taggable')->orderBy('sort', 'asc');
    }
}
