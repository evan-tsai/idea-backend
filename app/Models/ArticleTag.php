<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use App\Models\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
    use HasTranslations;

    protected $guarded = ['id'];

    protected $appends = ['trans'];

    protected $hidden = ['translations', 'pivot'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ArticleCategory', 'category_id', 'id');
    }

    public function articles()
    {
        return $this->morphedByMany('App\Models\Article', 'article_taggable')->orderBy('sort', 'asc');
    }
}
