<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductTagCategory extends Model
{
    protected $guarded = ['id'];

    /** relations */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function tags()
    {
        return $this->hasMany('App\Models\ProductTag', 'category_id', 'id')->withoutGlobalScopes();
    }
}
