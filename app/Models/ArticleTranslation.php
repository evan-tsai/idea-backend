<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleTranslation extends Model
{
    protected $guarded = ['id'];

    public function base()
    {
        return $this->belongsTo(Article::class, 'base_id', 'id');
    }

    public function editors()
    {
        return $this->morphMany('App\Models\Editor', 'editable')->orderBy('sort', 'asc');
    }
}
