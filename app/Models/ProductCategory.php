<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use App\Models\Traits\HasTranslations;
use App\Models\Traits\MultiLevelCategory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use MultiLevelCategory, HasTranslations;

    protected $guarded = ['id'];

    protected $appends = ['trans'];

    protected $hidden = ['translations'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')->orderBy('sort', 'asc');
    }
}
