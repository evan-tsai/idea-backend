<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
    protected $guarded = ['id'];

    /** relations */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function categories()
    {
        return $this->hasMany('App\Models\MediaCategory', 'type_id', 'id')->withoutGlobalScopes()->orderBy('sort', 'asc');
    }

    public function media()
    {
        return $this->hasMany('App\Models\Media', 'type_id', 'id')->withoutGlobalScopes()->orderBy('sort', 'asc');
    }
}
