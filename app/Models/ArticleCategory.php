<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $guarded = ['id'];

    /** relations */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function articles()
    {
        return $this->hasMany('App\Models\Article', 'category_id', 'id')->withoutGlobalScopes();
    }

    public function tags()
    {
        return $this->hasMany('App\Models\ArticleTag', 'category_id', 'id')->withoutGlobalScopes();
    }
}
