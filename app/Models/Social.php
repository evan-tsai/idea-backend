<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public static function getSocialTypes()
    {
        return collect([
            'Youtube',
            'Facebook',
            'Line',
            'Instagram',
        ]);
    }
}
