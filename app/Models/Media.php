<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use App\Models\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasTranslations;

    protected $guarded = ['id'];

    protected $appends = ['trans'];

    protected $hidden = ['translations'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function type()
    {
        return $this->belongsTo('App\Models\MediaType', 'type_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\MediaCategory', 'category_id', 'id');
    }
}
