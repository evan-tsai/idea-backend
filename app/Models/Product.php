<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use App\Models\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasTranslations;

    protected $guarded = ['id'];

    protected $appends = ['trans'];

    protected $hidden = ['translations'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
    }

    public function tags()
    {
        return $this->morphToMany('App\Models\ProductTag', 'product_taggable')->orderBy('product_taggables.sort', 'asc');
    }
}
