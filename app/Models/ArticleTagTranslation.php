<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleTagTranslation extends Model
{
    protected $guarded = ['id'];

    public function base()
    {
        return $this->belongsTo(ArticleTag::class, 'base_id', 'id');
    }
}
