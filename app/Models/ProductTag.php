<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use App\Models\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    use HasTranslations;

    protected $guarded = ['id'];

    protected $appends = ['trans'];

    protected $hidden = ['translations'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    /** relations */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ProductTagCategory', 'category_id', 'id');
    }

    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'product_taggable')->orderBy('sort', 'asc');
    }
}
