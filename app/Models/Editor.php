<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Editor extends Model
{
    public $guarded = ['id'];

    protected $hidden = ['editable_id', 'editable_type'];

    public function editable()
    {
        return $this->morphTo();
    }
}
