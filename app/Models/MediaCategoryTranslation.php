<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaCategoryTranslation extends Model
{
    protected $guarded = ['id'];

    public function base()
    {
        return $this->belongsTo(MediaCategory::class, 'base_id', 'id');
    }
}
