<?php

namespace App\Models;

use App\Models\Scopes\CustomerScope;
use App\Models\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasTranslations;

    protected $guarded = ['id'];

    protected $appends = ['trans'];

    protected $hidden = ['translations'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CustomerScope);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function type()
    {
        return $this->belongsTo('App\Models\ContentType', 'type_id', 'id');
    }
}
