<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createIdeaCustomer();
    }

    public function createIdeaCustomer()
    {
        $customer = Customer::create([
            'domain'      => config('admin.domain'),
            'logo'        => '',
            'name'        => '無極限資訊行銷有限公司',
            'default_lang' => 'tw',
            'status'      => true,
        ]);

        event(new \App\Events\CustomerCreated($customer));

        return $customer;
    }
}
