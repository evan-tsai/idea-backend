<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 'tw',
                'code' => 'zh-TW',
                'name' => '中文',
            ],
            [
                'id' => 'en',
                'code' => 'en-US',
                'name' => 'English',
            ],
        ];

        \DB::table('languages')->insert($items);
    }
}
