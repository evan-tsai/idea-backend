<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTagTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_tag_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('base_id')->index();
            $table->string('lang', 15)->index();
            $table->string('name')->nullable();
            $table->foreign('base_id')->references('id')->on('article_tags')->onDelete('cascade');
            $table->unique(['base_id', 'lang']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_tag_translations');
    }
}
