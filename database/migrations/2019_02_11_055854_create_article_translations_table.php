<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('base_id')->index();
            $table->string('lang', 15)->index();

            $table->string('name')->nullable();
            $table->string('image_path')->nullable();
            $table->string('author')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_description', 1024)->nullable();
            $table->string('seo_keyword', 1024)->nullable();

            $table->foreign('base_id')->references('id')->on('articles')->onDelete('cascade');
            $table->unique(['base_id', 'lang']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_translations');
    }
}
