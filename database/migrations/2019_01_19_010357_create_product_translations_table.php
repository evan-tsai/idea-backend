<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('base_id')->index();
            $table->string('lang', 15)->index();

            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('url')->nullable();
            $table->string('image_path')->nullable();
            $table->string('icon_path')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_description', 1024)->nullable();
            $table->string('seo_keyword', 1024)->nullable();

            $table->foreign('base_id')->references('id')->on('products')->onDelete('cascade');
            $table->unique(['base_id', 'lang']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
    }
}
