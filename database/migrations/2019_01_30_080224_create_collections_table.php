<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('image_path')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('video_url')->nullable();
            $table->string('url')->nullable();

            $table->tinyInteger('status')->default(1);
            $table->smallInteger('sort')->default(1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
